/*
 esp8266 SVD-Framework. Types definition.

 Copyright (c) 2020 Sergej Pfaffenrot. All rights reserved.

 Based on ESP8266 EEPROM library, part of standard
 esp8266 core for Arduino environment by Ivan Grokhotkov.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


#ifndef SVD_PROJOBJ_CFGSTRUCT_H
#define SVD_PROJOBJ_CFGSTRUCT_H

#include <Arduino.h>

typedef void(*event_pin_t)(uint8_t a_index, uint8_t a_pin) ;
typedef void(*event_handle_t)(String* a_html);
#define CALL_EVENT_PIN_(X) if(event_pin != NULL) event_pin( getIndex(), X)

#pragma pack(push, 1)
typedef struct {
  uint8_t Pin:            8;
  uint8_t InterruptMode:  3;    // how do set interrupt: on key down, on key up or any
  uint8_t ButtonMode:     1;    // which kind of button: tact-button or switch (with fixed state)
} opt_cbutton_t;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct {
  uint8_t Pin:            8;
  uint8_t InvertedSignal:      1;   // 
  uint8_t SetDefaultOn:   1;   // must be "on" on ste start  
} opt_crelay_t;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct {
  uint32_t timePeriod           ;   // 
  uint8_t SetDefaultOn:        1;   // must be "on" on ste start  
} opt_ctimer_t;
#pragma pack(pop)

#pragma pack(push, 1)
typedef union {
    uint8_t       haveHTML : 1;
 

  opt_cbutton_t cbutton;
  opt_crelay_t  crelay;
  opt_ctimer_t  ctimer;
} opt_objcommon_t;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct {
  uint8_t         deviceKind:     6;  // SVD_CPROGOBJKIND_* 
  opt_objcommon_t obj; 
} obj_options_t; 
#pragma pack(pop)

#define float2fixfloat(x)   {floor(x), ceil(100*(x-floor(x))) }
#define fixfloat2float(y)   (y.fl+(float)y.cl/100)

#pragma pack(push, 1)
typedef struct { 
    uint16_t fl:11; 
    uint8_t cl:7; 
} fixfloat_t;
#pragma pack(pop)

#pragma pack(push, 1)
typedef union{
  uint32_t      intVal:19;
  fixfloat_t    floatVal;
} operand_t;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct {
  uint8_t         cmd             ;
  uint8_t         senderContext    ;  // for filter of senders events
  uint8_t         senderStateIndex:     2; // the objects class must have the functions getStateInt(index) and getStateFloat(index)
  uint8_t         log_op           ; // logic operation
  uint8_t         valueKind:       1; // kind of operand: int or float
  operand_t       value;
} bind_options_t; 
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct {
  uint8_t idx_sender; 
  uint8_t idx_receiver; 
  bind_options_t bind_options; 
} bind_t;
#pragma pack(pop)


#define SVD_CPROGOBJKIND_CPINOUT     0
#define SVD_CPROGOBJKIND_CPININP     1
#define SVD_CPROGOBJKIND_CDHT        2
#define SVD_CPROGOBJKIND_CLED        3
#define SVD_CPROGOBJKIND_CDS18B20    4
#define SVD_CPROGOBJKIND_CDS18S10    5
#define SVD_CPROGOBJKIND_CBMP280     6
#define SVD_CPROGOBJKIND_CTIMER      7
#define SVD_CPROGOBJKIND_CMQTT       8

#define SVD_LOGOP_nop                 0
#define SVD_LOGOP_equ                 1
#define SVD_LOGOP_nequ                2
#define SVD_LOGOP_low                 3
#define SVD_LOGOP_lowequ              4
#define SVD_LOGOP_big                 5
#define SVD_LOGOP_bigequ              6

#define SVD_CONTEXT_NOTSET            0
#define SVD_CONTEXT_TIMER             1
#define SVD_CONTEXT_BUTTONSTATE       2


#endif
/*
#include <iostream>
#include <cmath>
//#define unit8_t unsgned char
using namespace std;

#define float2fixfloat(x)   {floor(x), ceil(100*(x-floor(x))) }
#define fixfloat2float(y)   (y.fl+(float)y.cl/100)

float f = 25.576, f1;

#pragma pack(push, 1)
typedef struct{uint16_t fl; uint8_t cl;} fixfloat_t;
#pragma pack(pop)

typedef union
{
    struct{
        uint8_t b1;
        uint8_t b2;
    };
    struct{
        uint16_t si1;
    };

} union_t;

union_t ut1;

fixfloat_t ft;



int main()
{
    ut1.b1 = 10;
    ut1.b2 = 21;
    ut1.si1 = 55;


    ft = float2fixfloat(f);
    f1 = fixfloat2float(ft);

    cout<<"Hello World" <<f<<" "<<f1<<endl;
    cout << sizeof(ft)<<" "<<sizeof(float)<<endl;

    return 0;
}



*/