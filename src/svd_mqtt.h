/*
 esp8266WiFi SVD-Framework. MQTT class.

 Copyright (c) 2020 Sergej Pfaffenrot. All rights reserved.

 Based on ESP8266 EEPROM library, part of standard
 esp8266 core for Arduino environment by Ivan Grokhotkov.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef SVD_MQTT_H_
#define SVD_MQTT_H_

#include <Arduino.h>
#include "svd_prjobj.h"
#include "svd_prjobj_cfgstruct.h"
#include <MQTT.h>
#include <ESP8266WiFi.h>
#include "esp8266WiFiStationCtrl.h"

#define SVD_MQTT_DEFAULTPORT        1883

#define SVD_MQTTSTATE_TRY2CONNECT       1
#define SVD_MQTTSTATE_NOTCONNECTED      0


using namespace std::placeholders;

class CMQTT : public CProgObj {
  protected:
    static uint8_t              static_index;
    uint8_t                     mqttstate = 0;
    String                      host;
    uint16_t                    port = SVD_MQTT_DEFAULTPORT;
    WiFiClient*                 wifiClient;
    MQTTClient*                 mqtt_client;
    String                      subTopic;
    String                      pubTopic;
    MQTTClientCallbackAdvanced  call_back = NULL;
    /*static*/ void             local_mqtt_callback(MQTTClient* client, char* topic, char* payload, int payload_length);
  public:
    MQTTClient*                 getMQTTClient(){ return mqtt_client; };
    bool                        sendMQTT( CProgObj* obj);
    bool                        sendMQTT_string( const char* a_str);
    void                        onMessageAdvanced(MQTTClientCallbackAdvanced cb);
  protected:
     void                       objectBindCall( uint8_t a_sender, bind_options_t bind_options);
   public:
     void                       mqtt_loop();
     void                       mqtt_begin();
     void                       doMQTTSetting( );
   public:

     bool                       is_dev_out(){ return true;};
     bool                       is_dev_in() { return true;};
     String                     getJSON();

     obj_options_t              getObjOptions();

     String                     getHTML();

     void                       setSubTopic(String a_str) { subTopic = a_str; }
     void                       setPubTopic(String a_str) { pubTopic = a_str; }
     void                       setHost(String a_str) { host = a_str; }
     void                       setPort(uint16_t a_port) { port = a_port; }

     void                       readDataFrom(WiFiStationCtrl* wifictrl);

     CMQTT(WiFiClient* a_wifiClient, uint16_t maxPay);

     CMQTT(WiFiClient* a_wifiClient, uint16_t maxPay, String a_host, uint16_t a_port);

     CMQTT( WiFiClient* a_wifiClient, uint16_t maxPay, String a_host, uint16_t a_port, String a_pubTopic, String a_subTopic)
        { 
            CProgObj::newProgObj(); 
            html_report     = true;
            static_index    = getIndex();
            host            = a_host;
            port            = a_port; 
            wifiClient      = a_wifiClient;
            subTopic        = a_subTopic;
            pubTopic        = a_pubTopic;
            mqtt_client     = new MQTTClient( maxPay );
            doMQTTSetting( );
        } 
};

#endif
