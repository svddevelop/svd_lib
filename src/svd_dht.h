/*
 esp8266WiFi SVD-Framework. DHTxx class.

 Copyright (c) 2020 Sergej Pfaffenrot. All rights reserved.

 Based on ESP8266 EEPROM library, part of standard
 esp8266 core for Arduino environment by Ivan Grokhotkov.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef SVD_DHT_H_
#define SVD_DHT_H_


#include "svd_prjobj.h"
#include "svd_prjobj_cfgstruct.h"
#include "DHT.h"


//#define DHT11 11  /**< DHT TYPE 11 */
//#define DHT12 12  /**< DHY TYPE 12 */
//#define DHT22 22  /**< DHT TYPE 22 */
//#define DHT21 21  /**< DHT TYPE 21 */
//#define AM2301 21 /**< AM2301 */


#define SVD_CDHT_MESSTRESHOLD   5000
#define SVD_CDHT_IDX_TEMP       0
#define SVD_CDHT_IDX_HUM        1

class CDHT : public CProgObj {
  protected:
     int8_t             pin = SVD_PO_NOTDEFINED;
     DHT                dht;
     float              temp;
     float              humidity;
     uint32_t           latest_millis = 0;
     uint8_t            stype;
   public:

     bool               is_dev_out(){ return false;};
     bool               is_dev_in(){ return true;};
     String             getJSON();
     String             getJSONDomoticz();
     String             getJSONHomeAssistant();
     obj_options_t      getObjOptions();
     void               setPin( int8_t a_pin){ pin = a_pin;  };
     DHT*               getDHT(){ return &dht; }
     //void               setDHT( DHT* a_dht){ dht = a_dht;}
     uint8_t            getSensorType(){ return stype; };
     void               setSensorType(uint8_t a_stype){ stype = a_stype; };

     float              getStateFloat(uint8_t a_index);
     float              getTemperature();
     float              getHumidity();
     void               getData();
     void               init();

     String              getHTML();
     bool                canJSON() { return true; };
     String              getClassName() { PGM_P name = PSTR("DHT"); return String(name); };

     CDHT( int8_t a_pin, uint8_t a_stype = DHT11)
        : pin(a_pin), stype(a_stype), dht(a_pin, a_stype, 6)
        { 
          CProgObj::newProgObj(); 
          html_report = true;
          //DHT::DHT( a_pin, a_stype, 6);
          //setPin( a_pin );
          //setSensorType( a_stype );
          //temp      = 0;
          //humidity  = 0;
          //setDHT( new DHT( a_pin, a_stype ) );
          //dht->begin(); 
        } 

};
#endif
