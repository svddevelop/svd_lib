/*
 esp8266 SVD-Framework. Abstract class of framework form components.

 Copyright (c) 2020 Sergej Pfaffenrot. All rights reserved.

 Based on ESP8266 EEPROM library, part of standard
 esp8266 core for Arduino environment by Ivan Grokhotkov.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "svd_prjobj.h"
#include <vector>
#include <ESP8266WebServer.h>
#include <Esp.h>
#include "svd_timer.h"
#include "svd_mqtt.h"
#include "svd_pininp.h"

#include <ArduinoJson.h>

const char c_bat_pow[]      PROGMEM = ",\"Battery\":%f";
const char c_index[]        PROGMEM = "index";
const char c_state[]        PROGMEM = "state";
const char c_pin[]          PROGMEM = "pin";
const char c_inverted[]     PROGMEM = "inverted";

const char c_json_domoticz[]  PROGMEM =  "{\"idx\":%d,\"name\":\"%s\",\"svalue\":\"%s\",\"nvalue\":%d; \"Battery\":%s}";

extern "C" {
ADC_MODE(ADC_VCC);
}



std::vector<CProgObj*> CProgObj::global_progobjList;

std::vector<bind_t> CProgObj::global_BindList;
  
std::vector<uint8_t>   CProgObj::global_pins;

bool  CProgObj::as_dz = false;
bool  CProgObj::as_ha = false;
findAliasByIdx_t      CProgObj::findAliasByIdx;
findIdxByAlias_t      CProgObj::findIdxByAlias;


////////////////////////////////////////////////////////////////////////////////////////////////////

String CProgObj::getChipName(){

   String  sDevName   = String( "IOT" ) + String( ESP.getChipId(), HEX );
   return  sDevName ;
}

int CProgObj::addGlobalBinding(CProgObj* a_sender, CProgObj* a_receiver, bind_options_t options){

  uint8_t ret = global_BindList.size();
  uint8_t sender = a_sender->getIndex(), receiver = a_receiver->getIndex();
  bind_t bind = {sender, receiver, options};
  global_BindList.push_back( bind );
  Serial.printf_P( PSTR("addGlobalBinding(%d, %d)\r\n"), sender, receiver); 
  debugBindOptions( bind.bind_options );
  return ret;
}

bool CProgObj::checkBindClaues( bind_t a_bind, uint8_t a_sender_context ){

  bool ret = (
                  ( index == a_bind.idx_sender )
                &&( a_bind.bind_options.senderContext == a_sender_context )
              );

  if (( ret )&&(  a_bind.bind_options.log_op != SVD_LOGOP_nop )){

    ret = false;
    
    Serial.println("checkBindClaues[1]");

    CProgObj* sender = getProgObjByIndex( a_bind.idx_sender );
    if ( a_bind.bind_options.log_op == SVD_LOGOP_equ ){
        if ( a_bind.bind_options.valueKind )
          ret = ( sender->getStateFloat( a_bind.bind_options.senderStateIndex ) == fixfloat2float(a_bind.bind_options.value.floatVal) );
        else
          ret = ( sender->getStateInt( a_bind.bind_options.senderStateIndex ) == a_bind.bind_options.value.intVal );
    }
  }
  Serial.printf_P( PSTR("checkBindClaues()=%"), ret );
  return ret;
}

void CProgObj::callBind(  uint8_t a_sender_context ){

  //CProgObj* obj_sender = getProgObjByIndex( a_sender );
  
  for( bind_t bind: global_BindList ){

    if ( checkBindClaues( bind, a_sender_context ) ){

      Serial.printf_P( PSTR("callBind(%d, %d)\r\n"), index, bind.idx_receiver); 
      debugBindOptions( bind.bind_options );
      CProgObj* obj_receiver = getProgObjByIndex( bind.idx_receiver );
      obj_receiver->objectBindCall( index, bind.bind_options );
    }
      
  }
}

bind_options_t CProgObj::buildBindOptions(uint8_t a_cmd, uint8_t a_senderContext
                                  , uint8_t a_senderStateIndex, uint8_t a_log_op, uint8_t a_valueKind
                                  , uint32_t a_intVal, float a_floatVal){
    bind_options_t ret;
    ret.cmd = a_cmd; ret.senderContext = a_senderContext; ret.senderStateIndex = a_senderStateIndex;
    ret.log_op = a_log_op; ret.valueKind = a_valueKind;
    if ( a_valueKind )
      ret.value.floatVal = float2fixfloat(a_floatVal);
    else
      ret.value.intVal = a_intVal;
    return ret;                                   
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

CProgObj* CProgObj::getProgObjByIndex( uint16_t a_idx ){

   for( CProgObj* i : global_progobjList ){
     
     uint16_t idx = (*i).getIndex();
     if ( idx == a_idx )
        return i;
   }

   return nullptr;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
  const char c_json_all1[]      PROGMEM = "{\"name\":\"";
  const char c_json_all2[]      PROGMEM = "\",";
  const char c_json_all3[]      PROGMEM = ":[";
  const char c_json_all4[]      PROGMEM = "]}";
  const char c_json_sensors[]   PROGMEM = "\"sensors\"";
  
String CProgObj::getJSONAll(){

   bool need_comma = false;
   String ret = FPSTR(c_json_all1) + getChipName() + FPSTR(c_json_all2) + FPSTR(c_json_sensors) + FPSTR(c_json_all3);

   
   for( CProgObj* i : global_progobjList ){
  
      Serial.printf_P(PSTR("JSONALL %d\r\n"), i->getIndex());

      String dev = (*i).getJSON();
      
      // If dev is emty, then don't need doing something
      if ( dev.length() != 0 ){
          if ( need_comma )
             ret += ',';
          need_comma = true;
      }
      //*
         
      ret += dev;
   }
   ret += FPSTR(c_json_all4);
   return ret;
}

uint8_t  CProgObj::getCMQTT_index(){

        for( CProgObj* i : global_progobjList ){

        obj_options_t oo = i->getObjOptions();
        if ( oo.deviceKind == SVD_CPROGOBJKIND_CMQTT ) {

           return ((CTimer*)i)->getIndex();
        }
          
        }
    return SVD_PO_NOTDEFINED;
}


void   CProgObj::global_put_JSON( String a_json){

    PGM_P c_brodcastreq = PSTR("\"name\":\"*\"");
    String brodcastrequest = String(c_brodcastreq);

    
    int pos = a_json.indexOf(brodcastrequest);

    if (pos < 1)  
        pos = a_json.indexOf( "\"" + getChipName() + "\"" );

    Serial.printf_P( PSTR("\r\n::global_put_JSON pos=%d \r\n"), pos);
      //Serial.printf_P( PSTR("::global_put_JSON haveChipName=%d \r\n"), haveChipName);
      if ( pos > 0){

        //check if some asked about configuration 
        pos = a_json.indexOf( c_json_sensors );
        if ( pos > 0 ){

            CMQTT* mqtt = (CMQTT*)getProgObjByIndex( getCMQTT_index() );
            if ( mqtt != NULL )
              //mqtt->sendMQTT_string( getJSONAll().c_str() );// <- it did not work. I think the problem with lengt of message
                for (CProgObj* i : global_progobjList) {
                    mqtt->sendMQTT_string(i->getJSON().c_str());
                    /*
                    if (CProgObj::canDomoticzJSON())
                        mqtt->sendMQTT_string(i->getJSONDomoticz().c_str());
                    if (CProgObj::canHomeAssistantJSON())
                        mqtt->sendMQTT_string(i->getJSONHomeAssistant().c_str());
                        */
                }
        //*
        } else
            // found, which object must process it
            for( CProgObj* i : global_progobjList ){

                bool found = i->putJSON( a_json );
            }
      }

  
}

/*
auto  CProgObj::getJsonDocValue( StaticJsonDocument* a_doc, const char* a_key  PROGMEM, auto a_default ){

  auto val = a_default;
  String key = String( a_key);
  if ( doc.containsKey( key ) ){
     val = doc[ key ].as<auto>();
  return val;
  
}
*/
////////////////////////////////////////////////////////////////////////////////////////////////////////

CProgObj& CProgObj::newProgObj(){

  index = global_progobjList.size(); 
  global_progobjList.push_back( this );  
  init_global_pins();
  incBatInJson = true;
  return *this;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

bool CProgObj::init_global_pins()
{
  if ( global_pins.size() > 0) return false;
  
  global_pins.push_back( 0 );
  global_pins.push_back( 2 );
  global_pins.push_back( 4 );
  global_pins.push_back( 5 );
  global_pins.push_back( 12 );
  global_pins.push_back( 13 );

  return true;
}

void    CProgObj::global_set_pin( CProgObj* a_obj, uint8_t a_pin, uint8_t a_val){

  digitalWrite( a_pin, a_val );
  Serial.printf_P( PSTR("global_set_pin(obj:%d, pin:%d, val:%d)\r\n"), a_obj->getIndex(), a_pin, a_val );

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

String  CProgObj::getHTML() {

    String ret = "";
    if (extHandlerHTML != NULL)
        extHandlerHTML(&ret);
    return ret;
}

String  CProgObj::getHTMLAll() {

    return getHTMLtext() + getHTMLfooter();
}

String  CProgObj::getHTMLfooter() {

    PGM_P c_footer = PSTR("</body></html>");

    return String(c_footer);
}

String  CProgObj::getHTMLtext() {

    String ret = "\r\n";
    for (CProgObj* i : global_progobjList) {

        obj_options_t oo = i->getObjOptions();
        //if (oo.obj.haveHTML) {
          if ( i->canShowHtml() ){

            String dh = i->getHTML();
            //Serial.printf_P(PSTR("html[%d]:%s\r\n"), i->getIndex(), dh.c_str());

            ret += "<div class='dev'>";
            ret += dh;
            ret += "</div>\r\n";
        }
    }
 
    return ret;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

float CProgObj::getBatteryPower(){

  int vcc = ESP.getVcc();//readvdd33();
  //float  f_vcc = 100 * vcc/3595;
  float  f_vcc = vcc/1024;
  return f_vcc;
}

String  CProgObj::getBatteryPowerS(){
  
  if ( includeBatteryInJSON() ){

    float f_bp = getBatteryPower();
    PGM_P c_bat  = PSTR(",\"Battery\":");
    
    String s_bp = String(c_bat) + String( f_bp );
    return s_bp;
    
  }  else
        return "";
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
void   CProgObj::global_loop(){

     for( CProgObj* i : global_progobjList ){

        obj_options_t oo = i->getObjOptions();
        if ( oo.deviceKind == SVD_CPROGOBJKIND_CTIMER ) {

           ((CTimer*)i)->timer_loop();
        }
        
        if ( oo.deviceKind == SVD_CPROGOBJKIND_CMQTT ) {
           ((CMQTT*)i)->mqtt_loop();
        }

        if ( oo.deviceKind == SVD_CPROGOBJKIND_CPININP ) {
           ((CPinInp*)i)->pininp_loop();
        }


     }
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void   CProgObj::debugBindOptions( bind_options_t bind_options ){

    Serial.printf_P( PSTR("{cmd:%d; senderContext:%d; senderStateIndex:%d, log_op:%d, valueKind:%d, intVal:%d, floatVal:%f}\r\n")
        , bind_options.cmd
        , bind_options.senderContext
        , bind_options.senderStateIndex 
        , bind_options.log_op 
        , bind_options.valueKind 
        , bind_options.value.intVal 
        , bind_options.value.floatVal 
        
        );
}
