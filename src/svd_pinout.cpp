/*
 esp8266 SVD-Framework. The class for simply output over
 any pin of SoC, like relay, summer or LED.

 Copyright (c) 2020 Sergej Pfaffenrot. All rights reserved.

 Based on ESP8266 EEPROM library, part of standard
 esp8266 core for Arduino environment by Ivan Grokhotkov.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "svd_prjobj.h"
#include "svd_pinout.h"
#include "svd_prjobj_cfgstruct.h"
#include "svd_pininp.h"
#include <ArduinoJson.h>

const char c_json[]         PROGMEM = "{\"name\":\"%s\",\"object\":\"pinout\",\"index\":%d,\"state\":%d,\"inverted\":%d,\"pin\":%d%s}";


uint8_t CPinOut::switch_state(){

  switch( state ) {

    case SVD_REL_STATE_OFF:
      setState( getValueOn() );
      break;
      
    case SVD_REL_STATE_ON:
      setState( getValueOff() );
      
  }

   return state;
}

String CPinOut::getJSON(){

  //If pin does not assigned then do not get something
  if ( pin == SVD_PO_NOTDEFINED )
      return "";
  //*/
  char temp[ 500 ];
  temp[0] = 0;
  String s_bp = getBatteryPowerS();
  

  
  
  snprintf_P ( temp, sizeof(temp), c_json, getChipName().c_str(), index, state, inverted_signal, pin, s_bp.c_str() );
  return String(temp);
}

bool   CPinOut::putJSON( String a_json){
  
   bool ret = false;
   StaticJsonDocument<550> doc;
   deserializeJson(doc, a_json.c_str(), a_json.length() );

  //const char* name = doc["name"].as<char*>();
  //String docName = String( doc["name"].as<char*>() );

  String key = String(c_index);
  if (doc.containsKey(key)) {
      int idx = doc[key].as<int>();
      ret = (idx == getIndex());
  }
   if ( ! ret ) return ret;
    

   key = String(c_pin);
   if (doc.containsKey(key)) {
       uint8_t j_pin = doc[key].as<int>();
       Serial.printf_P( PSTR("CPinOut::putJSON::SetPin(%d)\r\n"), j_pin );
       setPin(j_pin);
   }

   //uint8_t val = getJsonDocValue( &doc, c_state, getState() );
   key = String(c_state);
   if (doc.containsKey(key)) {
       uint8_t j_state = doc[key].as<int>();
       Serial.printf_P( PSTR("CPinOut::putJSON::setState(%d)\r\n"), j_state );
       setState(j_state);
   }

   
   if ( a_json.indexOf( "\"" + String( c_inverted) + "\"") > 0){
     uint8_t j_invtd = doc[ String(c_inverted) ].as<int>(); 
       Serial.printf_P( PSTR("CPinOut::putJSON::setInvertedSignal(%d)\r\n"), j_invtd );
     setInvertedSignal( (bool)j_invtd );
   }
    
   return ret;
}

uint8_t CPinOut::getValueOn(){
  if (inverted_signal)
    return SVD_REL_STATE_OFF;
  else
    return SVD_REL_STATE_ON; 
}

uint8_t CPinOut::getValueOff(){

  if (inverted_signal)
    return SVD_REL_STATE_ON; 
  else
    return SVD_REL_STATE_OFF;   
}


void CPinOut::setInvertedSignal( bool a_ps){

   inverted_signal = a_ps;
   Serial.printf_P(PSTR("%d[PO].setInvertedSignal(%d)\r\n"), index, a_ps);
}

void CPinOut::setPin( int8_t a_pin){

   pin = a_pin;
   if ( pin > SVD_PO_NOTDEFINED )
     pinMode( pin, OUTPUT);
}

void       CPinOut::setRelayOn(){
  
  setState( getValueOn() );
}

void       CPinOut::setRelayOff(){

  Serial.printf_P( PSTR("%d[PO]:setRelayOff() getValueOff():%d inverted_signal=%d\r\n"), index, getValueOff(), inverted_signal );

  setState( getValueOff() );
  
}

bool       CPinOut::isRelayOn(){

  return state == getValueOn();
}

bool       CPinOut::isRelayOff(){
  
  return state == getValueOff();
}

void       CPinOut::setState( uint8_t a_state){

  state_before = state;

  state = a_state;
  if ( pin > SVD_PO_NOTDEFINED ){
    
      Serial.printf_P( PSTR("%d[PO].setState( %d )\r\n"), getIndex(), state );
      setPin( pin );
      //digitalWrite( pin, state );
      global_set_pin( this, pin, state );
      //Serial.printf_P( PSTR("digitalWrite(%d,%d) ps:%d  getValueOff:%d getValueOn:%d\r\n"), pin, state, inverted_signal, getValueOff(), getValueOn() );

      CALL_EVENT_PIN_( pin );
  }
}

void CPinOut::objectBindCall( uint8_t a_sender, bind_options_t bind_options){

   Serial.printf_P( PSTR("[R]objectBindCall(%d, %d)\r\n"), index, pin);
   debugBindOptions( bind_options );
   CProgObj* sender = getProgObjByIndex(a_sender);
   obj_options_t sender_options = sender->getObjOptions();
   
   //if ( sender_options.deviceKind == SVD_CPROGOBJKIND_CPININP)
   //{
    
     /*CPinInp* btn_ = (CPinInp*)sender;
     if ( btn_->getButtonMode() == SVD_BUTTONMODE_SWITCH ) {

        if ( btn_->isButtonOn() )
          setRelayOn();
        else
          setRelayOff();
     }
     else //SVD_BUTTONMODE_BUTTON
       switch_state();
   }*/

     if (bind_options.cmd == SVD_REL_STATE_ON){
 
                          setRelayOn();
     }
                          
     if (bind_options.cmd == SVD_REL_STATE_OFF){

                          setRelayOff();
     }
                          
     if (bind_options.cmd == SVD_REL_STATE_SWITCH ){
      
                          switch_state();
     }               
        
     
   //}
  
}

obj_options_t    CPinOut::getObjOptions(){

  obj_options_t ret;
  
  ret.deviceKind                  = SVD_CPROGOBJKIND_CPINOUT;
  ret.obj.crelay.Pin              = pin;
  ret.obj.crelay.InvertedSignal   = inverted_signal;
  ret.obj.crelay.SetDefaultOn     = doByDefaultOn;
  ret.obj.haveHTML                = canShowHtml();
  return ret;
}


int32               CPinOut::getStateInt(uint8_t a_idx) {

    if (a_idx == 0) return getState();

    return SVD_PO_NOTDEFINED;
}

float               CPinOut::getStateFloat(uint8_t a_idx) {

    if (a_idx == 0) return getState();
    return SVD_PO_NOTDEFINED;
}

int32               CPinOut::getStateBeforeInt(uint8_t a_idx) {

    if (a_idx == 0) return state_before;
    return SVD_PO_NOTDEFINED;
}

float               CPinOut::getStateBeforeFloat(uint8_t a_idx) {

    if (a_idx == 0) return state_before;
    return SVD_PO_NOTDEFINED;

}



String  CPinOut::getHTML() {

    String ret = "";
    if (extHandlerHTML != NULL)
        extHandlerHTML(&ret);
    else {

        PGM_P c_html = PSTR("<span class=sw><label class='switch'>"
            "<input type='checkbox' id='po%d' onchange='{"
            "st=this.checked;if(st==true)sti=0;else sti=1;ag(\"obj?index=%d&state=\"+sti,function(c){});};'"
            " checked>"
            "<span class='slider round'></span></label></span>\r\n"
        );
        char temp[800];
        snprintf_P(temp, sizeof(temp), c_html, index, index);
        ret = temp;

    }
    return ret;
}