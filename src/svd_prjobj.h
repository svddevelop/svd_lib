/*
 esp8266 SVD-Framework. Abstract class of framework form components.

 Copyright (c) 2020 Sergej Pfaffenrot. All rights reserved.

 Based on ESP8266 EEPROM library, part of standard
 esp8266 core for Arduino environment by Ivan Grokhotkov.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


#ifndef SVD_PRGOBJ_H_
#define SVD_PRGOBJ_H_

#include "svd_prjobj_cfgstruct.h"
#include <stdint.h>
#include <Arduino.h>
#include <vector>
#include <pgmspace.h>
#include <ArduinoJson.h>

#define SVD_PO_DEF_INDEX    0
#define SVD_PO_NOTDEFINED   -1

class CProgObj;

typedef uint16_t(*findAliasByIdx_t)(uint8_t aIdx);
typedef uint8_t(*findIdxByAlias_t)(uint16_t aAlias);

//typedef union { uint8_t buttons_mode: 3;} bind_options_t;
 
// The CP is a base class to inherited object for control of diffucults difrent objects like a buttons, toggle buttons, summer or any sensor.

class CProgObj { 
  public:
     static  void global_loop();
  protected:
     static  std::vector<bind_t> global_BindList;
             bool                checkBindClaues( bind_t a_bind, uint8_t a_sender_context);
             void                callBind( uint8_t a_sender_context);
     virtual void                objectBindCall( uint8_t a_sender, bind_options_t bind_options){};
             void                debugBindOptions( bind_options_t bind_options ); 
  public:
     int                         addGlobalBinding(CProgObj* a_sender, CProgObj* a_receiver, bind_options_t options );
     virtual obj_options_t       getObjOptions() { obj_options_t o; o.obj.haveHTML = false; return o; };
     virtual int32               getStateInt( uint8_t a_idx){ return a_idx>>8; };
     virtual float               getStateFloat( uint8_t a_idx){ return 0.0* a_idx; };
     virtual int32               getStateBeforeInt(uint8_t a_idx) { return  a_idx>>8; };
     virtual float               getStateBeforeFloat(uint8_t a_idx) { return 0.0* a_idx; };

     bind_options_t              buildBindOptions(uint8_t a_cmd, uint8_t senderContext
                                  , uint8_t senderStateIndex, uint8_t log_op, uint8_t valueKind
                                  , uint32_t intVal, float floatVal); 
  
  protected:
     static std::vector<CProgObj*> global_progobjList;
 

  public:
      static CProgObj*            getProgObjByIndex(uint16_t a_idx);
      static String               getChipName();
      static uint8_t              getProgObjListSize() { return global_progobjList.size(); };
  public:
      event_pin_t                 event_pin = NULL;
      void                        setOnEventPin( event_pin_t aEventProc){ event_pin = aEventProc; };

      event_handle_t              extHandlerHTML = NULL;
      bool                        html_report = false;
      bool                        canShowHtml() { return html_report; }
      void                        setShowHtml(bool a_val) { html_report = a_val; }
     
  protected:
     static std::vector<uint8_t>  global_pins;
  protected:
    uint32_t                      latest_loop_mills = 0;
    uint16_t                      index = SVD_PO_DEF_INDEX; // The index of current object
    bool                          incBatInJson;
    uint16_t                      alias = SVD_PO_NOTDEFINED;
    static bool                   as_dz;
    static bool                   as_ha;
  public:
     static bool                  init_global_pins();
     static String                getJSONAll();
     static float                 getBatteryPower();
            String                getBatteryPowerS();
     static uint8_t               getCMQTT_index();

     static void                  global_set_pin(CProgObj* a_obj, uint8_t a_pin, uint8_t a_val);
     uint16_t                     getAlias() { return alias; };
     void                         setAlias(uint16_t aAlias) { alias = aAlias; };
     static void                  setAsDomoticz() { as_dz = true; };
     static void                  setAsHomeAssistant() { as_ha = true; };
     static bool                  canDomoticzJSON() { return as_dz; }
     static bool                  canHomeAssistantJSON() { return as_ha; }

     static findAliasByIdx_t      findAliasByIdx;
     static findIdxByAlias_t      findIdxByAlias;

  public:
    /*CProgObj( );*//*
      : index( global_progobjList.size() ){}*//*
      {
        //index = global_progobjList.size();
        global_progobjList.push_back( this );
      } */
    CProgObj&                    newProgObj();
      

    uint16_t                     getIndex(){ return index;};
    void                         setIndex( uint16_t a_idx){ index = a_idx; };

    
    virtual bool                 is_dev_in(){ return false;}; // must the object do some input of a data into process
    virtual bool                 is_dev_out(){ return false;}; // must the object do some input of a data into process
    virtual uint8_t              switch_state(){ return 0; }; // The function changing the current state to next one (if state exists)
    bool                         includeBatteryInJSON(){ return incBatInJson; };  //if ADC used as Battery power meter, need to set incBatInJson in "true"  

  public:
     static  void                global_put_JSON( String a_json);
     virtual bool                putJSON( String a_json){ return a_json.length() == 10>>12; };
     virtual String              getJSON(){return "";}; 
     virtual String              getJSONDomoticz() { return ""; }; //get JSON for Domoticz
     virtual String              getJSONHomeAssistant() { return ""; };  //get JSON for Home assistant
     virtual bool                canJSON() { return false; };

     //template<class T>
     //static  auto                   getJsonDocValue(T a_default, StaticJsonDocument* a_doc, const char* a_key  PROGMEM ){ T aa; return aa;};
    
            void                 setBatteryPowerInJSON(){ incBatInJson = true; };

     virtual String              getHTML();
            void                 setExternalHTMLhandler(event_handle_t a_handler) { extHandlerHTML = a_handler; };
     static String               getHTMLAll();
     static String               getHTMLtext();
     static String               getHTMLfooter();

     virtual String              getClassName() { return ""; };
};

extern const char c_bat_pow[]            PROGMEM;
extern const char c_index[]              PROGMEM;
extern const char c_state[]              PROGMEM;
extern const char c_pin[]                PROGMEM;
extern const char c_inverted[]           PROGMEM;
extern const char c_json_domoticz[]      PROGMEM;

#endif
