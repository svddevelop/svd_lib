/*
 esp8266WiFi SVD-Framework. DHTxx class.

 Copyright (c) 2020 Sergej Pfaffenrot. All rights reserved.

 Based on ESP8266 EEPROM library, part of standard
 esp8266 core for Arduino environment by Ivan Grokhotkov.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "svd_dht.h"
#include "svd_prjobj.h"
#include "svd_prjobj_cfgstruct.h"

const char c_json[]  PROGMEM = "{\"name\":\"%s\",\"object\":\"dht\",\"index\":\"%d\""
                               ",\"temp\":\"%2.1f\",\"humidity\":\"%2.1f\",\"stype\":\"%d\""
                               ",\"pin\":%d%s}";
//const char c_json_dom[]  PROGMEM = //"{\"name\":\"%s\",\"object\":\"dht\",\"index\":\"%d\""
////",\"temp\":\"%2.1f\",\"humidity\":\"%2.1f\",\"stype\":\"%d\""
////",\"pin\":%d%s}";
//                               "{\"idx\":%d,\"name\":\"%s\",\"svalue\":\"%0.1f;%0.1f;0\",\"nvalue\":0}";
//const char c_stype[] PROGMEM = "stype";

const char c_json_dom[]  PROGMEM = "%0.1f;%0.1f;0";

void CDHT::init(){

    //dht = new DHT(pin, stype);
    temp = 0;
    humidity = 0;
}

String    CDHT:: getJSON(){
  //If pin does not assigned then do not get something
  if ( pin == SVD_PO_NOTDEFINED )
      return "";
  //*/

//  getStateFloat( SVD_CDHT_IDX_TEMP );
  
  char temp[ 500 ];
  temp[0] = 0;
  String s_bp = getBatteryPowerS();
   
  snprintf_P ( temp, sizeof(temp), c_json
                  , getChipName().c_str()
                  , index, getTemperature(), getHumidity()
                  , getSensorType()
                  , pin
                  , s_bp.c_str()
              );
  return String(temp);
}

String    CDHT::getJSONDomoticz() {
    //If pin does not assigned then do not get something
    if (pin == SVD_PO_NOTDEFINED)
        return "";
    //*/

  //  getStateFloat( SVD_CDHT_IDX_TEMP );

    char temp[500];
    temp[0] = 0;
    String s_bp = getBatteryPowerS();

    uint16_t alias = -1;
    if ((*CProgObj::findAliasByIdx) != NULL)
        alias = CProgObj::findAliasByIdx(index)
        ;
    snprintf_P(temp, sizeof(temp), c_json_dom
        , getTemperature(), getHumidity()
    );
    String th_dom = String(temp);

    snprintf_P(temp, sizeof(temp), c_json_domoticz
        , alias
        , getChipName().c_str()
        , th_dom.c_str()
        , 0 //getSensorType()
        //, pin
        , s_bp.c_str()
    );
    return String(temp);
}

String    CDHT::getJSONHomeAssistant() {
    //If pin does not assigned then do not get something
    if (pin == SVD_PO_NOTDEFINED)
        return "";
    //*/

  //  getStateFloat( SVD_CDHT_IDX_TEMP );

    char temp[500];
    temp[0] = 0;
    String s_bp = getBatteryPowerS();

    snprintf_P(temp, sizeof(temp), c_json
        , getChipName().c_str()
        , index, getTemperature(), getHumidity()
        , getSensorType()
        , pin
        , s_bp.c_str()
    );
    return String(temp);
}
/*bool   CDHT::putJSON( String a_json){
  
   bool ret = false;
   StaticJsonDocument<550> doc;
   deserializeJson(doc, a_json.c_str(), a_json.length() );

  //const char* name = doc["name"].as<char*>();
  //String docName = String( doc["name"].as<char*>() );

  if ( a_json.indexOf( "\"" + String( c_index) + "\"") > 0){
     int idx = doc[ String(c_index) ].as<int>();
     ret = (idx == getIndex() );
   }
   if ( ! ret ) return ret;
    

   //Serial.printf( c_show_name, idx );
   if ( a_json.indexOf( "\"" + String( c_state) + "\"") > 0){
     uint8_t j_state = doc[ String(c_state) ].as<int>(); 
     setState( j_state );
   }
   
   if ( a_json.indexOf( "\"" + String( c_bmode) + "\"") > 0){
     uint8_t j_bm = doc[ String(c_bmode) ].as<int>(); 
     setButtonMode( j_bm );
   }
    
   if ( a_json.indexOf( "\"" + String( c_imode) + "\"") > 0){
     uint8_t j_im = doc[ String(c_imode) ].as<int>(); 
     setInterruptMode( j_im );
   }
   
   return ret;
}*/

void CDHT::getData(){
  
        dht.begin();
        float f = dht.readTemperature(false);
        if ( isnan( f ) ) f = 0;
        else
          temp = f;
        f = dht.readHumidity();
        if ( isnan( f ) ) f = 0;
        else
          humidity = f;
        Serial.printf_P( PSTR("CDHT::getData() T:%f H:%\r\n"), temp, humidity);
  
}

float      CDHT::getStateFloat(uint8_t a_index){
    float ret = 0;
    uint32_t rest = millis() - latest_millis;

    if (     (!( pin == SVD_PO_NOTDEFINED ))
           &&(( rest > SVD_CDHT_MESSTRESHOLD )||(latest_millis == 0))
        ){

          getData();

          latest_millis = millis();
    }
    if ( a_index == SVD_CDHT_IDX_TEMP ) ret = temp;
    if ( a_index == SVD_CDHT_IDX_HUM )  ret = humidity;

    return ret; 
}

float      CDHT::getTemperature(){
  
   return getStateFloat( SVD_CDHT_IDX_TEMP );
}
     
float      CDHT::getHumidity(){
  
   return getStateFloat( SVD_CDHT_IDX_HUM );  
}


obj_options_t    CDHT::getObjOptions(){

    obj_options_t       ret;
    ret.deviceKind      = SVD_CPROGOBJKIND_CDHT;
    ret.obj.haveHTML    = canShowHtml();
    return              ret;
  
}




String  CDHT::getHTML() {

    PGM_P c_html  = PSTR(

        "<div><i class=\"fas fa-thermometer-half\">Temperature</i>:<span id='t%d'> %3.1f </span></div>\r\n"
        "<div><i class=\"fas fa-cloud-rain\">Humidity:</i><span id='h%d'> %2.0f </span></div>\r\n"

        "<script>"
        "setTimeout(function(){\r\n"       
        "ag('json?obj=%d', function(d){\r\n"       
        "document.getElementById('h%d').innerHTML=d['humidity'] + ' %%';\r\n"
        "document.getElementById('t%d').innerHTML=d['temp'] + ' °C';\r\n"
        "}, 5*60000);\r\n"
        "});</script>\r\n" 

    );

    String ret = "";
    if (extHandlerHTML != NULL)
        extHandlerHTML(&ret);
    else {

        getData();

        //uint8_t idx = getIndex();

        char temp[500];
        snprintf_P(temp, sizeof(temp), c_html, index, this->temp, index, this->humidity, index, index, index);
        ret = temp;

    }
    return ret;
}