/*
 esp8266 SVD-Framework. The class for simply input from
 any pin of SoC, like button, switch or one pins sensor.
 It could be use the interrupt or loop-process.

 Copyright (c) 2020 Sergej Pfaffenrot. All rights reserved.

 Based on ESP8266 EEPROM library, part of standard
 esp8266 core for Arduino environment by Ivan Grokhotkov.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef SVD_PININP_H_
#define SVD_PININP_H_

#include "svd_prjobj.h"

#define   SVD_BUT_STATE_OFF     0
#define   SVD_BUT_STATE_ON      1
#define   SVD_BUT_DEF_STATE     SVD_BUTL_STATE_OFF

#define  SVD_NOINTERRUPT        -1
#define  SVD_BUTTON_DOWN        0x01
#define  SVD_BUTTON_UP          0x02
#define  SVD_BUTTON_ANY         0x03

#define  SVD_BUTTONMODE_SWITCH   0x00
#define  SVD_BUTTONMODE_BUTTON   0x01

class CPinInp;

class CPinInp : public CProgObj {
  protected:
     bool       inverted_signal = true;
     int8_t     pin = SVD_PO_NOTDEFINED;

     // button_mode == SVD_BUTTONMODE_BUTTON : это срабатывает как обычная кнопка 
     //                и при  SVD_BUTTON_ANY  будет срабатывать 2 раза= нажатие и отпускание
     // button_mode == SVD_BUTTONMODE_SWITCH : работа при переключении = каждое состояние индивидуально.       
     uint8_t    button_mode = SVD_BUTTONMODE_BUTTON;

     int8_t     int_mode = SVD_BUTTON_UP;

     uint8_t    state;
     uint8_t    state_before;

     obj_options_t getObjOptions();
   private:
     bool                flip;
     ICACHE_RAM_ATTR     void                internal_handle();
public:

    bool                 is_dev_in() { return true; };
    uint8_t              getState();
    void                 setState(uint8_t a_state);
    String               getJSON();
    bool                 putJSON(String a_json);
    //uint8_t              getValueOn();
    //uint8_t              getValueOff();  
   // void                 setInvertedSignal( bool a_ps);
    void                 setPin(int8_t a_pin);
    void                 setButtonMode(uint8_t aBtnMode);
    uint8_t              getButtonMode() { return button_mode; };
    void                 setInterruptMode( int8_t aIntMode);
    int8_t               getInterruptMode() { return int_mode; };
    bool                 isButtonOn() { return state == 1; };
    bool                 isButtonOff() { return state == 0; };

    void                 pininp_loop();
     
     int32               getStateInt(uint8_t a_idx);
     float               getStateFloat(uint8_t a_idx);
     int32               getStateBeforeInt(uint8_t a_idx);
     float               getStateBeforeFloat(uint8_t a_idx);

     bool                canJSON() { return true; };
     String              getClassName() { PGM_P name = PSTR("PinInp"); return String(name); };


     CPinInp( int8_t a_pin = SVD_PO_NOTDEFINED )
        : flip(false)
        { 
          CProgObj::newProgObj(); 
          html_report = false;
          setPin( a_pin ); 
        } 
     CPinInp( int8_t a_pin, int8_t a_intMode)
         : flip(false)
        { 
          CProgObj::newProgObj(); 
          html_report = false;
          setPin( a_pin ); 
          setButtonMode(0);
          setInterruptMode(a_intMode);
        }
};

#endif
