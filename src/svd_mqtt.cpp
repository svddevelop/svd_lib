/*
 esp8266WiFi SVD-Framework. MQTT class.

 Copyright (c) 2020 Sergej Pfaffenrot. All rights reserved.

 Based on ESP8266 EEPROM library, part of standard
 esp8266 core for Arduino environment by Ivan Grokhotkov.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "svd_mqtt.h"
#include "svd_prjobj_cfgstruct.h"
#include <FunctionalInterrupt.h>


uint8_t    CMQTT::static_index;

const char c_mqtt_msg1[]         PROGMEM= "{2}CMQTT::sendMQTT(%d) mqtt.connected()=%d wifi.conn=%d\r\n";
const char c_mqtt_msg2[]         PROGMEM= "CMQTT::sendMQTT(%d) mqtt_client isn't connected mqttstate:%d";
const uint16_t c_defaulte_mqtt_port = 1883;
const char c_default_topic_in[]  PROGMEM = "iot/in";
const char c_default_topic_out[] PROGMEM = "iot/out";
const char c_no_host[]           PROGMEM = "MQTT don't have a host";

void CMQTT::readDataFrom(WiFiStationCtrl* wifictrl) {

    //if ( WIFICTRL != NULL )
    String s = wifictrl->getMQTTHost();
    if (s.length() > 0)
        setHost(s);

    uint16_t port = wifictrl->getMQTTPort();
    if ((port > 0) & (port < 65500))
        setPort(port);
    else {
        //wifictrl.setMQTTPort(c_mqtt_port);
        //setPort(c_mqtt_port);
    }

    s = wifictrl->getMQTTHost();
    if (s.length() > 0)
        setHost(s);
    else {
        //wifictrl.setMQTTHost(String(c_mqtt_host));
        //setHost(c_mqtt_host);
    }

    s = wifictrl->getMQTTTopicIn();
    if (s.length() > 0)
        setSubTopic(s);
    else {
        //setSubTopic(String(c_mqtt_topin));
        //setMQTTTopicIn(c_mqtt_topin);
    }

    s = wifictrl->getMQTTTopicOut();
    if (s.length() > 0)
        setPubTopic(s);
    else {
        //setPubTopic(String(c_mqtt_topout));
        //setMQTTTopicOut(c_mqtt_topout);
    }
}

bool  CMQTT::sendMQTT_string( const char* a_str){
  
  bool   ret;

  ret = mqtt_client->connected();
  if ( ret ) {
      Serial.printf_P( c_mqtt_msg1, SVD_PO_NOTDEFINED, ret, wifiClient->connected());

      Serial.println( pubTopic + ":" + a_str );

      ret = mqtt_client->publish( pubTopic, a_str  );

      Serial.printf_P(PSTR("CMQTT::sendMQTT_string:return % d\r\n"), ret);

  } 
  else
    Serial.printf_P( c_mqtt_msg2, SVD_PO_NOTDEFINED,  mqttstate );
  return ret;
  
}

bool   CMQTT::sendMQTT( CProgObj* obj){

  bool   ret;
  String json = obj->getJSON();

  ret = sendMQTT_string(json.c_str());

  /*
  ret = mqtt_client->connected();

  if ( ret ) {

      Serial.printf_P(c_mqtt_msg1, obj->getIndex(), ret, wifiClient->connected());

      Serial.println( pubTopic + ":" + json );

      ret = mqtt_client->publish( pubTopic, json  );

  } 
  else
    Serial.printf_P( c_mqtt_msg2, obj->getIndex(),  mqttstate );
  */

  if (canDomoticzJSON()) {

      json = obj->getJSONDomoticz();

      if (json.length() > 0)
          ret = sendMQTT_string(json.c_str());
  }
  if (canHomeAssistantJSON()) {

      json = obj->getJSONHomeAssistant();

      if (json.length() > 0)
          ret = sendMQTT_string(json.c_str());
  }

  return ret;
}

void        CMQTT::onMessageAdvanced(MQTTClientCallbackAdvanced cb){

           call_back = cb;
}

void        CMQTT::local_mqtt_callback(MQTTClient* client, char* topic, char* payload, int payload_length){

            Serial.printf_P( PSTR("CMQTT::local_mqtt_callback:%s\r\n"), payload );
            CMQTT* inst = (CMQTT*) getProgObjByIndex( static_index );
            if ( inst->call_back != NULL )
                inst->call_back( client, topic, payload, payload_length );

            String json = String( payload );
            global_put_JSON( json );
  
}

void       CMQTT::mqtt_begin(){

    Serial.printf_P(PSTR("%d[CMQTT] host:%s, port:%d, sub:%s; pub:%s\r\n"), getIndex(), host.c_str(), port, subTopic.c_str(), pubTopic.c_str());

    if (WIFICTRL != NULL)
      readDataFrom( WIFICTRL );

    if (host.length() == 0)
        Serial.println(c_no_host);
    else
      mqtt_client->begin(host.c_str(), port,  *wifiClient );
  
}

void        CMQTT::doMQTTSetting() {

    if (host.length() == 0)
        Serial.println(c_no_host);
    else {

        mqtt_client->onMessageAdvanced(std::bind(&CMQTT::local_mqtt_callback, this, _1, _2, _3, _4));

        mqtt_client->subscribe(subTopic);
    }
}

void       CMQTT::objectBindCall( uint8_t a_sender, bind_options_t bind_options){
  
}




void       CMQTT::mqtt_loop(){
  
    static uint32_t time_millis = millis();

    delay(1);
    
    mqtt_client->loop();

 
    if ( mqtt_client->connected() ) {


        mqttstate = SVD_MQTTSTATE_NOTCONNECTED;

      if  ( ! mqtt_client->loop() )
        Serial.println( F(" mqtt.loop= 0") );
      
    }
    else     {

      uint32_t tme =  (time_millis / 1000);
      if (tme & 1) {
          Serial.print('_');
      }
      tme = (time_millis / 10000);
      if ( tme & 1)
         Serial.println( "" );

        mqttstate = SVD_MQTTSTATE_TRY2CONNECT;

        mqtt_client->connect( getChipName().c_str(), "", "" );
        doMQTTSetting();

    }
    delay(1);  
}

String     CMQTT::getJSON(){

    String ret = "{\"name\":\"" + getChipName() + "\",\"state\":";
    ret += (int)mqtt_client->connected();
    ret += ",\"object\":\"CMQTT\"";
    ret += ",\"host\":" + host;
    ret += ",\"port\":" + String(port);
    ret += ",\"sub\":" + subTopic;
    ret += ",\"pub\":" + pubTopic;
    IPAddress ip = wifiClient->localIP();
    ret += ",\"ip\":\"" + String(ip[0]) + "." + String(ip[1]) + "." + String(ip[2]) + "." + String(ip[3]) + "\"";
    ret += " }";
    return ret;
}

obj_options_t    CMQTT::getObjOptions(){


    obj_options_t ret;
    ret.deviceKind        = SVD_CPROGOBJKIND_CMQTT;
    ret.obj.haveHTML      = canShowHtml();
    return ret;
  
}


String  CMQTT::getHTML() {

    PGM_P c_html = PSTR ("<div><i class=\"fas fa-broadcast-tower\">MQTT:<input type='checkbox' id='po%d' %s></i>"
        "</div><script>setInterval(function(){document.getElementById('po%d').checked=%d;},60000);</script>");
    PGM_P  c_checked = PSTR ("checked");
    
    String ret = "";
    if (extHandlerHTML != NULL)
        extHandlerHTML(&ret);
    else {

        String sst = "";
        if (! mqtt_client->connected())
            sst += "un";
        sst += c_checked;

        char temp[500];
        snprintf_P(temp, sizeof(temp), c_html, index, sst.c_str(), index, mqtt_client->connected() );
        ret = temp;

    }
    return ret;
}


CMQTT::CMQTT(WiFiClient* a_wifiClient, uint16_t maxPay)
{
    CProgObj::newProgObj();
    html_report = true;
    static_index = getIndex();
    host = "";
    port = c_defaulte_mqtt_port;
    wifiClient = a_wifiClient;
    subTopic = String(c_default_topic_in);
    pubTopic = String(c_default_topic_out);
    mqtt_client = new MQTTClient(maxPay);
    doMQTTSetting();
}

CMQTT::CMQTT(WiFiClient* a_wifiClient, uint16_t maxPay, String a_host, uint16_t a_port)
{
    CProgObj::newProgObj();
    html_report = true;
    static_index = getIndex();
    host = a_host;
    port = a_port;
    wifiClient = a_wifiClient;
    subTopic = String(c_default_topic_in);
    pubTopic = String(c_default_topic_out);
    mqtt_client = new MQTTClient(maxPay);
    doMQTTSetting();
}