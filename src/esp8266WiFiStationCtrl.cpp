/*
 esp8266WiFiStationControl.cpp - esp8266 do a manage of your
 ssid/password, automatically connection to external access point
 or start the internal acces point with conficuration web-server.

 Copyright (c) 2020 Sergej Pfaffenrot. All rights reserved.

 Based on ESP8266 EEPROM library, part of standard
 esp8266 core for Arduino environment by Ivan Grokhotkov.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


#include <user_interface.h>
#include "esp8266WiFiStationCtrl.h"
#include <Esp.h>
 //#include <ESP8266WiFi.h>
 //#include <ESP8266WebServer.h>
#define WEBSERVER_H

#include <ESP8266mDNS.h>
#include <DNSServer.h>
#include <ESP8266HTTPUpdateServer.h>
#include <ESP_EEPROM.h>
#include "svd_prjobj.h"
#include <Esp.h>


extern "C" {
#include "user_interface.h"
}

const char upd_path[] = "/upd";

//static WiFiStationCtrl* WIFICTRL;

//++++++++++++++++++++++++++++++++++++++++++++++

uint16_t eeprom_addr = 0;
uint16_t eeprom_len = 0;
uint16_t eeprom_crc = 0;

std::vector<alias_t> WiFiStationCtrl::global_aliasList;


void _write_(void* ptr, uint8_t ptrsize) {

    while (ptrsize > 0) {

        uint8_t c = *(uint8_t*)(ptr);

        EEPROM.put(eeprom_addr, c);

        eeprom_crc += c;
        if ((eeprom_addr & 0x0F) == 0) Serial.printf_P(PSTR("\r\n%4.4x :"), eeprom_addr);
        Serial.printf_P(PSTR(" %2.2x"), c);

        ptr += 1;

        eeprom_addr += 1;

        ptrsize--;

    }
    //Serial.printf_P(PSTR("EEPROM OFFSET:%d(0x%x)\r\n"), eeprom_addr, eeprom_addr);

}//void _write_( void* ptr, uint8_t ptrsize)

void _read_(void* ptr, uint8_t ptrsize) {

    uint8_t* ptr_ = (uint8_t*)ptr;
    while (ptrsize > 0) {

        uint8_t c; EEPROM.get(eeprom_addr, c);

        //Serial.printf_P(PSTR(" addr: %x "), ptr_);

        //*(uint8_t*)(ptr) = c;
        *ptr_ = c;

        if ((eeprom_addr & 0x0F) == 0) Serial.printf_P(PSTR("\r\n%4.4x :"), eeprom_addr);
        Serial.printf_P(PSTR(" %2.2x"), c);

        eeprom_crc += c;

        ptr_ += 1;
        //ptr_++;

        eeprom_addr += 1;

        ptrsize--;

    }

    //Serial.printf(c_dbg_addr, eeprom_addr );

}//void _read_( void* ptr, uint8_t ptrsize)

uint16_t write_String(String* astr) {


    uint8_t len = astr->length();


    //EEPROM.put( eeprom_addr++, len);
    _write_(&len, sizeof(len));

    for (uint8_t i = 0; i < len; i++) {

        uint8_t c = astr->c_str()[i];//Serial.printf("%d ", c);

        //EEPROM.put(eeprom_addr++, c );
        _write_(&c, sizeof(c));
    }

    EEPROM.put(eeprom_addr++, 0);

    return eeprom_addr;
}

String read_String() {

    uint8_t tmp[32];

    uint8_t len = 0;

    _read_(&len, sizeof(len));

    if (len > sizeof(tmp)) {

        Serial.printf_P(PSTR("[wsl] EEPROMStr at %4.4x has len=%d and not inialized!\r\n"), eeprom_addr, len);
        return "";
    }

    for (uint8_t i = 0; i <= len; i++) {

        _read_(&tmp[i], sizeof(uint8_t));
    }

    return String((char*)tmp);
}
//++++++++++++++++++++++++++++++++++++++++++++++



/*ESP8266WebServer* WiFiStationCtrl::getWebServer(){

    return (ESP8266WebServer*)&server;
}*/

void WiFiStationCtrl::readEEPROMaliasList() {

    uint8_t counts;
    _read_(&counts, sizeof(counts));
    Serial.printf_P(PSTR("\r\naliases:count=%d\r\n"), counts);
    if (counts > 0) {

        while (counts > 0) {

            alias_t alias;
            _read_(&alias, sizeof(alias));
            Serial.printf_P(PSTR("{idx:%d;alias:%d}\r\n"), alias.idx, alias.alias);
            global_aliasList.push_back(alias);
            counts--;

        }
    }
}

void WiFiStationCtrl::writeEEPROMaliasList() {

    uint8_t counts = global_aliasList.size();
    Serial.printf_P(PSTR("\r\naliases:count=%d\r\n"), counts);
    _write_(&counts, sizeof(counts));
    if (counts > 0) {

        while (counts > 0) {

            alias_t alias = global_aliasList[counts - 1];
            Serial.printf_P(PSTR("{idx:%d;alias:%d}\r\n"), alias.idx, alias.alias);
            _write_(&alias, sizeof(alias));
            counts--;

        }
    }
}


void  WiFiStationCtrl::write2EEPROM() {

    if (isPoorChip()) return;

    Serial.println(F("[wsc]write2EEPROM"));
    EEPROM.begin(512);
    eeprom_addr = sizeof(eeprom_crc) + sizeof(eeprom_len);
    eeprom_crc = 0;

    write_String(&mqtt_host);
    Serial.printf_P(PSTR(" mqtt_host:%s\r\n"), mqtt_host.c_str());


    _write_(&mqtt_port, sizeof(mqtt_port));
    Serial.printf_P(PSTR(" mqtt_port:%d\r\n"), mqtt_port);


    write_String(&mqtt_usr);
    Serial.printf_P(PSTR(" mqtt_usr:%s\r\n"), mqtt_usr.c_str());

    write_String(&mqtt_pass);
    Serial.printf_P(PSTR(" mqtt_pass:%s\r\n"), mqtt_pass.c_str());

    write_String(&mqtt_ti);
    Serial.printf_P(PSTR(" mqtt_topicin:%s\r\n"), mqtt_ti.c_str());

    write_String(&mqtt_to);
    Serial.printf_P(PSTR(" mqtt_topicout:%s\r\n"), mqtt_to.c_str());

    char mqtt_extend = 0;
    if (mqtt_domoticz) mqtt_extend |= 0x1;
    if (mqtt_ha) mqtt_extend |= 0x2;
    _write_(&mqtt_extend, sizeof(mqtt_extend));

    Serial.printf_P(PSTR("[w]usr_data size %d\r\n"), sizeof(eeprom_usr_data[0]) * WFSC_UDCOUNT_);

    _write_(eeprom_usr_data, sizeof(eeprom_usr_data[0]) * WFSC_UDCOUNT_);
    ////Serial.printf_P(PSTR("\r\neeprom_usr_data:"));
    ////for (uint8_t i = 0; i < WFSC_UDCOUNT_; i++)
    ////    Serial.printf_P(PSTR("[%d]%x "), i, eeprom_usr_data[i]);

    //for (uint8_t i = 0; i < WFSC_UDCOUNT_; i++)
    //    _write_(&eeprom_usr_data[i], 1);
    Serial.println("");

    writeEEPROMaliasList();

    eeprom_len = eeprom_addr - sizeof(eeprom_crc) - sizeof(eeprom_len);
    eeprom_addr = 0;
    uint16_t crc = eeprom_crc;
    EEPROM.put(0, eeprom_len);
    EEPROM.put(sizeof(eeprom_len), eeprom_crc);



    Serial.printf_P(PSTR("[wsc]eeprom:len=%d, crc=%d\r\n"), eeprom_len, crc);

    EEPROM.commitReset();
    Serial.println(F("[wsc]commit"));
    delay(500);
}

bool  WiFiStationCtrl::read2EEPROM() {

    if (isPoorChip()) return true;

    Serial.println(F("[wsc]read2EEPROM"));

    EEPROM.begin(512);
    eeprom_addr = 0;
    uint16_t saved_crc = 0, saved_len = 0;
    _read_(&saved_crc, sizeof(saved_crc));
    _read_(&saved_len, sizeof(saved_len));
    //eeprom_addr = sizeof(eeprom_crc) + sizeof(eeprom_len);
    eeprom_crc = 0;


    mqtt_host = read_String();
    Serial.printf_P(PSTR(" mqtt_host:%s\r\n"), mqtt_host.c_str());


    _read_(&mqtt_port, sizeof(mqtt_port));
    Serial.printf_P(PSTR(" mqtt_port:%d\r\n"), mqtt_port);


    mqtt_usr = read_String();
    Serial.printf_P(PSTR(" mqtt_usr:%s\r\n"), mqtt_usr.c_str());

    mqtt_pass = read_String();
    Serial.printf_P(PSTR(" mqtt_pass:%s\r\n"), mqtt_pass.c_str());

    mqtt_ti = read_String();
    Serial.printf_P(PSTR(" mqtt_topic in:%s\r\n"), mqtt_ti.c_str());

    mqtt_to = read_String();
    Serial.printf_P(PSTR(" mqtt_topic out:%s\r\n"), mqtt_to.c_str());

    char mqtt_extend = 0;
    _read_(&mqtt_extend, sizeof(mqtt_extend));
    mqtt_domoticz = (mqtt_extend & 1) > 0;
    if (mqtt_domoticz) CProgObj::setAsDomoticz();
    mqtt_ha = (mqtt_extend & 2) > 0;
    if (mqtt_ha) CProgObj::setAsHomeAssistant();

    void* ud_ref = (void*)eeprom_usr_data;
    Serial.printf_P(PSTR("usr_data *  %x\r\n"), ud_ref);

    Serial.printf_P(PSTR("usr_data size %d\r\n"), sizeof(eeprom_usr_data[0]) * WFSC_UDCOUNT_);

    _read_(eeprom_usr_data, sizeof(eeprom_usr_data[0]) * WFSC_UDCOUNT_);

    //Serial.printf_P(PSTR("\r\neeprom_usr_data:"));
    //for (uint8_t i = 0; i < WFSC_UDCOUNT_; i++)
    //    Serial.printf_P(PSTR("0x%x "), eeprom_usr_data[i]);

    readEEPROMaliasList();

    Serial.printf_P(PSTR("\r\nEEPROM OFFSET:%d(0x%x)\r\n"), eeprom_addr, eeprom_addr);
    Serial.printf_P(PSTR("EEPROM crc:0x%x == 0x%x, len:%d == %d\r\n"), saved_crc, eeprom_crc, saved_len, eeprom_len);

    return ((saved_crc == eeprom_crc) & (saved_len == eeprom_len));
}

void WiFiStationCtrl::WriteToUserData(uint8_t a_ud[]) {

    for (uint8_t i = 0; i < sizeof(a_ud); i++) {

        setEEPROMUserData(i, a_ud[i]);
    }
}

void WiFiStationCtrl::ReadFromUserData(uint8_t a_ud[]) {

    for (uint8_t i = 0; i < sizeof(a_ud); i++) {

        a_ud[i] = getEEPROMUserData(i);
    }
}

uint16_t WiFiStationCtrl::findAliasByIdx(uint8_t aIdx) {

    for (alias_t alias : global_aliasList) {

        if (alias.idx == aIdx)
            return alias.alias;
    }
    return 0xFFFF;
}

uint8_t WiFiStationCtrl::findIdxByAlias(uint16_t aAlias) {

    for (alias_t alias : global_aliasList) {

        if (alias.alias == aAlias)
            return alias.idx;
    }
    return 0xFF;
}



WebServer* WiFiStationCtrl::getWebServer() {

    return server;
}

bool   WiFiStationCtrl::isPoorChip() {

    uint32_t fs = ESP.getFlashChipSize();
    return !(fs > (512 * 1024));
}

String WiFiStationCtrl::getHostName() {

    return hostName;
}

String WiFiStationCtrl::getMQTTHost() {

    return mqtt_host;
}

void     WiFiStationCtrl::setMQTTHost(String a_host) {

    mqtt_host = String(a_host.c_str());
}

uint16_t WiFiStationCtrl::getMQTTPort() {

    return mqtt_port;
}

void     WiFiStationCtrl::setMQTTPort(uint16_t a_port) {

    mqtt_port = a_port;
}

String  WiFiStationCtrl::getMQTTUsr() {

    return mqtt_usr;
}

void     WiFiStationCtrl::setMQTTUsr(String a_usr) {

    mqtt_usr = String(a_usr.c_str());
}

String  WiFiStationCtrl::getMQTTPw() {

    return mqtt_pass;
}

void     WiFiStationCtrl::setMQTTPw(String a_pass) {

    mqtt_pass = String(a_pass.c_str());
}

String  WiFiStationCtrl::getMQTTTopicIn() {

    return mqtt_ti;
}

void     WiFiStationCtrl::setMQTTTopicIn(String a_str) {

    mqtt_ti = String(a_str.c_str());
}
String  WiFiStationCtrl::getMQTTTopicOut() {

    return mqtt_to;
}

void     WiFiStationCtrl::setMQTTTopicOut(String a_str) {

    mqtt_to = String(a_str.c_str());
}



WiFiStationCtrl::WiFiStationCtrl()
    : wifiClient(), httpUpdater() {

    CProgObj::findAliasByIdx = &WiFiStationCtrl::findAliasByIdx;
    CProgObj::findIdxByAlias = &WiFiStationCtrl::findIdxByAlias;

    PGM_P c_iot = PSTR("IOT");
    PGM_P c_ti = PSTR("iot/in");
    PGM_P c_to = PSTR("iot/out");

    Serial.begin(115200);
    Serial.printf_P(PSTR("\r\n\r\n[wsc]Welcome!\r\n[wsc]ver. %s\r\n"), getBuildVersion().c_str());


    hostName = String(c_iot) + String(ESP.getChipId(), HEX);

    mqtt_ti = String(c_ti);
    mqtt_to = String(c_to);
    mqtt_port = 1883;
    mqtt_usr = "";
    mqtt_pass = "";
    mqtt_host = "";
    mqtt_domoticz = false;
    mqtt_ha = false;


    server = NULL;

    have_ssid = false;

    timeout = 0;

    for (uint8_t i = 0; i < WFSC_UDCOUNT_; i++)
        eeprom_usr_data[i] = 0;
    /*for (uint8_t i = 0; i < WFSC_UDCOUNT_; i++)
        Serial.printf_P(PSTR("1 ud[%d]=%d\r\n"), i, eeprom_usr_data[i]);*/


    WIFICTRL = this;

    global_aliasList.clear();
    read2EEPROM();
}

void WiFiStationCtrl::setTimeout(uint a_timeout) {

    timeout = a_timeout; //timeout in sec. if timeout == 0 then will AP active
    Serial.printf_P(PSTR("[wsc] setTimeout(%d)\r\n"), a_timeout);

}

const char* WiFiStationCtrl::getFlashSSID() {

    return (const char*)&wifi_config.ssid[0];
}

const char* WiFiStationCtrl::getFlashPsk() {

    return (const char*)&wifi_config.password[0];
}

bool WiFiStationCtrl::setWiFiSSID( const char* a_ssid){

    strcpy( (/*const*/ char*)&wifi_config.ssid , a_ssid);
    return true;
}

bool WiFiStationCtrl::setWiFiPWD( const char* a_pwd){

    strcpy( (/*const*/ char*)&wifi_config.password , a_pwd);
    return true;

}

bool WiFiStationCtrl::is_have_ssid() {

    return have_ssid;
}

bool WiFiStationCtrl::compare_ssid(const char* a_ssid) {

    return strcmp(a_ssid, getFlashSSID());
}

bool WiFiStationCtrl::get_connparams_fromFlash() {

    delay(100);
    Serial.println( F("[w]get_connparams_fromFlash 1") );
    delay(100);
    wifi_station_get_config_default(&wifi_config);
   Serial.println( F("[w]get_connparams_fromFlash 2") );
   delay(100);
 
    if ((strlen(getFlashSSID()) == 0) || (strlen(getFlashPsk()) < 8))

        return false;

    Serial.printf_P(PSTR("[wsc]Saved SSID:%s,"), &wifi_config.ssid);
    Serial.printf_P(PSTR("pw:%s\r\n"), &wifi_config.password);
    delay(100);

    return true;

}

bool WiFiStationCtrl::make_connection_fromFlash() {

    wifi_set_opmode(STATION_MODE);
    WiFi.persistent(false);
    WiFi.begin(getFlashSSID(), getFlashPsk());
    delay(100);
    return true;

}

void WiFiStationCtrl::save_connparams_toFlash(String ssid, String passwd) {

    WiFi.persistent(true);
    WiFi.begin(ssid, passwd);
    WiFi.persistent(false);
    delay(100);

}

uint64_t WiFiStationCtrl::getEEPROMUserData(uint8_t a_idx) {

    return eeprom_usr_data[a_idx];
}

void     WiFiStationCtrl::setEEPROMUserData(uint8_t a_idx, uint64_t a_data) {

    eeprom_usr_data[a_idx] = a_data;
}

bool WiFiStationCtrl::set_ssid_active() {

    if ((strlen(getFlashSSID()) == 0) || (strlen(getFlashPsk()) < 8))

        return false;


    int n = WiFi.scanNetworks();
    Serial.println(F("[wsc]Scann..."));
    if (n == 0) {

        return false;

    }
    else {

        for (int i = 0; i < n; ++i) {

            // Print SSID and RSSI for each network found

            Serial.printf_P(PSTR("[wsc]%d :"), i + 1);

            Serial.println(WiFi.SSID(i));

            if (WiFi.SSID(i).equals(getFlashSSID()))

                return true;

        }
    }

    return false;
}

bool WiFiStationCtrl::is_ssid_active() {

    wifi_station_get_config_default(&wifi_config);

    /*
    if ((strlen(getFlashSSID()) == 0) || (strlen(getFlashPsk()) < 8))

        return false;


    int n = WiFi.scanNetworks();
    Serial.println(F("[wsc]Scann..."));
    if (n == 0) {

        return false;

    }
    else {

        for (int i = 0; i < n; ++i) {

            // Print SSID and RSSI for each network found

            Serial.printf_P(PSTR("[wsc]%d :"), i + 1);

            Serial.println(WiFi.SSID(i));

            if (WiFi.SSID(i).equals(getFlashSSID()))

                return true;

        }
    }

    return false;*/

    return set_ssid_active();
}

uint8_t WiFiStationCtrl::addHandler(html_handler_t a_handler) {

    uint8_t ret = (uint8_t)ext_handlers.size();
    ext_handlers.push_back(a_handler);
    return ret;
}

String WiFiStationCtrl::getBuildVersion() {

    String nr = String(__DATE__);
    nr.remove(6, 1);
    nr.remove(1, 3);

    String ret = nr;

    nr = String(__TIME__);
    nr.remove(5, 1);
    nr.remove(2, 1);

    ret += nr;

    return ret;

}



void WiFiStationCtrl::start_accesspoint_with_confSrv(bool withCaptive) {

    Serial.println(F("[wsc]START AP"));
    Serial.println( hostName );
    delay(10);

    PGM_P c_ap_password = PSTR("12345678");
    WiFi.mode(WIFI_AP);
    WiFi.softAP(hostName, c_ap_password);

    DNSServer* dnsServer;

    if (withCaptive) {
        IPAddress ip(192, 168, 4, 1);
        dnsServer = new DNSServer();
        dnsServer->start(WFSC_DNS_PORT_, "*", ip);
        Serial.print(F("[wsc]DnsServer IP:")); Serial.println(wifiClient.localIP());
    }

    if (server == NULL)
        server = new ESP8266WebServer(80);

    //server = new ESP8266WebServerTemplate( 80 );
    server->on("/", std::bind(&WiFiStationCtrl::handleWiFiCfg, this));
    server->on("/wificfg", std::bind(&WiFiStationCtrl::handleWiFiCfg, this));
    server->on("/logo", std::bind(&WiFiStationCtrl::handleLogo, this));
    server->on("/save", std::bind(&WiFiStationCtrl::handleSaveToFlash, this));
    server->onNotFound(std::bind(&WiFiStationCtrl::handleNotFound, this));
    httpUpdater.setup(server, upd_path);
    /*
      for (html_handler_t n : ext_handlers) {
         server->on( n.addr, &n.handlerProc );
      }
    */
    server->begin();
    Serial.println(F("[wsc]HTTP server started"));
    MDNS.begin(hostName);
    MDNS.addService("http", "tcp", 80);

    uint32_t _millis = 0xFFFFFFFF;
    if (timeout > 0)
        _millis = millis() + timeout * 1000000;

    while (true) {

        if (withCaptive)
            dnsServer->processNextRequest();

        server->handleClient();

        MDNS.update();

        delay(100);

        if (millis() > _millis) {

            Serial.println(F("[wsc]Timeout HTTP"));
            delay(100);
            break;
        }

    }

    ESP.reset();
}

void WiFiStationCtrl::webServerSetup(WebServer* a_server) {

    server = a_server;

    a_server->on("/wificfg", std::bind(&WiFiStationCtrl::handleWiFiCfg, this));
    a_server->on("/save", std::bind(&WiFiStationCtrl::handleSaveToFlash, this));
    a_server->on("/logo", std::bind(&WiFiStationCtrl::handleLogo, this));
    a_server->on("/json", std::bind(&WiFiStationCtrl::handleJSON, this));
    a_server->on("/obj", std::bind(&WiFiStationCtrl::handleObjects, this));
    a_server->on("/mqttcfg", std::bind(&WiFiStationCtrl::handleMqtt, this));
    a_server->on("/savemqtt", std::bind(&WiFiStationCtrl::handleSaveMQTTToFlash, this));
    a_server->on("/fwu", std::bind(&WiFiStationCtrl::handleUpdate, this));
    a_server->onNotFound(std::bind(&WiFiStationCtrl::handleNotFound, this));
    httpUpdater.setup(a_server, upd_path);

    Serial.println(F("[wsc]WebServer setted up"));
    delay(10);
    //return a_server;

}

/*ESP8266WebServer* WiFiStationCtrl::startWebServer( int port ){

  server.reset(new ESP8266WebServer( port ));
  WebServerSetup( ( ESP8266WebServer*) &server );
  server->begin();
  Serial.println( F("[wsc]HTTP server started") );
  MDNS.begin( hostName );
  return (ESP8266WebServer*)&server;
}*/

void WiFiStationCtrl::setup(const bool a_PrintDot, bool a_withCaptive) {

    Serial.printf_P(PSTR("[wsc]Host:%s\r\n"), hostName.c_str());

    read2EEPROM();

    /*for (uint8_t i = 0; i < WFSC_UDCOUNT_; i++)
        Serial.printf_P(PSTR("2 ud[%d]=%d\r\n"), i, eeprom_usr_data[i]);*/


    have_ssid = get_connparams_fromFlash();

    bool is_ssidactive = false;


    if (have_ssid)

        is_ssidactive = is_ssid_active();


    Serial.printf_P(PSTR("[wsc]have_ssid:%d\r\n"), have_ssid);
    Serial.printf_P(PSTR("[wsc]is_ssid_active:%d\r\n"), is_ssidactive);

    if ((have_ssid) && (is_ssidactive)) {

        Serial.println(F("[wsc]do connection..."));

        make_connection_fromFlash();

        wifi_connect(a_PrintDot);


    }
    else {  // when don't have a SSID or SSID-Poit isn't active ---

     // start WiFi as AccessPoint and Web-Server for configuration 

     //wifi_station_save_connparams("aaa", "bbb" ); // <<--- simulation of save of configuration
        start_accesspoint_with_confSrv(a_withCaptive);

    }

}

void WiFiStationCtrl::setup_direct(const char* a_ssid, const char* a_pwd, const bool a_PrintDot) {

  setWiFiSSID( a_ssid );
  setWiFiPWD( a_pwd );
  Serial.println( getFlashSSID() );
  //Serial.println( wifictrl.getFlashPsk() );

  if ( set_ssid_active() ) {

     make_connection_fromFlash();
     wifi_connect( a_PrintDot );
     Serial.print(F("IP:")); Serial.println( wifiClient.localIP() );

  } else {

      Serial.printf_P( PSTR("SSID %s not found!\r\n"), getFlashSSID() );

  }
}

void WiFiStationCtrl::wifi_connect(const bool a_PrintDot) {

    if (WiFi.status() != WL_CONNECTED) {

        //WiFi.config(IPAddress(ip_static), IPAddress(ip_gateway), IPAddress(ip_subnet), IPAddress(ip_dns));

        int Attempt = 0;

        while (WiFi.status() != WL_CONNECTED) {

            if (a_PrintDot)
                Serial.print(F("."));
            //Serial.print(Attempt);
            delay(100);
            Attempt++;
            if (Attempt == 240 )

            {

                Serial.println("");
                Serial.println(F("[wsc]Could not connect to WIFI"));
                if (local_onwificonnect == NULL) {

                    ESP.restart();
                }
                else {
                    DNSServer* dnsServer;
                    dnsServer = new DNSServer();
                    dnsServer->start(WFSC_DNS_PORT_, "*", WiFi.localIP());
                    MDNS.begin(hostName);
                    MDNS.addService("http", "tcp", 80);

                    local_onwificonnect(this);
                }
                delay(200);

            }

        }

        Serial.println();
        Serial.print(F("[wsc]WiFi connected"));
        Serial.print(F(" IP: "));
        Serial.println(WiFi.localIP());

    }

}



//////////////////////////////////////////////////////////////////////////////////////////////////////

const char c_json_text[] PROGMEM = "text/json";
const char c_text_html[] PROGMEM = "text/html";
const char c_text_plain[] PROGMEM = "text/plain";
const char c_reset[] PROGMEM = "<body><h1>Reset</h1><script>document.ready(window.setTimeout(location.href=\"http://%s/?\",5000));</script></body>";


void WiFiStationCtrl::handleUpdate() {

    PGM_P c_firmware = PSTR("firmware");
    if (server->hasArg(c_firmware)) {

    }
    else {

        String html = getHTMLHeader();

        html += getHTMLCSS();

        PGM_P html1 = PSTR(
            "<form method='POST' action='/upd' enctype='multipart/form-data'>\n\
    Firmware:<br>\n\
    <input type='file' accept='.bin,.bin.gz' name='firmware'>\n\
    <input type='submit' value='Update Firmware'>\n\
    </form></body></html>\n\
    "
        );
        PGM_P html2 = PSTR("<label style='font-size:xx-small;font-style:italic;'>ver. ");
        PGM_P html3 = PSTR("</label><br>");

        html += html2;

        html += getBuildVersion();

        html += html3;
        html += html1;
        server->send(200, c_text_html, html);
    }
}

void WiFiStationCtrl::handleObjects() {

    //Serial.printf_P(PSTR("\r\n/obj? !"));

    String json = "{\"name\":\"";
    json += getHostName();
    json += "\"";

    int8_t idx = -1;

    for (int i = 0; i < server->args(); i++) {

        json += ",\"" + server->argName(i) + "\":";
        json += server->arg(i);

        if (server->argName(i).equals("index"))
            idx = server->arg(i).toInt();
    }

    json += "}";
    //Serial.printf_P(PSTR("\r\n/obj? << %s"), json.c_str());

    CProgObj::global_put_JSON(json);
    json = "{}";
    if (idx >= 0) {

        CProgObj* obj = CProgObj::getProgObjByIndex(idx);
        if (obj != NULL)
            json = obj->getJSON();
    }

    server->send(200, c_json_text, json);
    Serial.printf_P(PSTR("\r\n/obj? >> %s"), json.c_str());
    delay(100);
}

void WiFiStationCtrl::handleJSON() {

    PGM_P txt_obj = PSTR("obj");

    String str = server->arg(txt_obj);
    int8_t obj_id = str.toInt();

    str = "{}";
    if (obj_id == SVD_PO_NOTDEFINED) {


        str = CProgObj::getJSONAll();
    }
    else {

        CProgObj* obj = CProgObj::getProgObjByIndex(obj_id);
        if (obj != NULL)
            str = obj->getJSON();

    }
    server->send(200, c_json_text, str);
    //Serial.println( txt );
    delay(100);

}

String WiFiStationCtrl::getHTMLLogo() {

    PGM_P html_logo = PSTR("<div><img src=\"/logo\"></div>");

    return html_logo;
}

void WiFiStationCtrl::handleMqtt() {

    //PGM_P html_logo = PSTR( "<div><img src=\"/logo\"></div>" );
    PGM_P html_h1 = PSTR("<div style=\"margin-bottom:2em;\"><h1>Set the MQTT setting:</h1></div><form method=\"post\" action=\"/savemqtt\">");

    PGM_P c_mh = PSTR("mh");
    PGM_P c_mp = PSTR("mp");
    PGM_P c_mu = PSTR("mu");
    PGM_P c_mpw = PSTR("mpw");
    PGM_P c_mti = PSTR("mti");
    PGM_P c_mto = PSTR("mto");
    PGM_P c_mcd = PSTR("mcd");
    PGM_P c_mcha = PSTR("mcha");
    PGM_P c_on = PSTR("on");
    PGM_P c_checked = PSTR("checked");

    //PGM_P html_st1 = PSTR("<div class=ap><i class=\"fas fa-network-wired\"></i></i><label onclick=\"c(this)\">");
    //PGM_P html_st2 = PSTR("</label></div>");

    PGM_P html_st4 = PSTR("<div><i class=\"fas fa-network-wired\"></i><input placeholder=\"MQTT host\" id=s name=\"mh\" value=\"");
    PGM_P html_st5 = PSTR("\"></div>");
    PGM_P html_st6 = PSTR("<div><i class=\"fas as fa-plug\"></i><input placeholder=\"Port\" id=p name=\"mp\" value=\"");

    PGM_P html_st8 = PSTR("<div><i class=\"fas as fa-user\"></i><input placeholder=\"User\" id=p name=\"mu\" value=\"");

    PGM_P html_st9 = PSTR("<div><i class=\"fas as fa-key\"></i><input placeholder=\"Password\" id=p name=\"mpw\" value=\"");
    PGM_P html_st10 = PSTR("<div><i class=\"fas as fa-arrow-down\"></i><input placeholder=\"Topic in\" id=ti name=\"mti\" value=\"");
    PGM_P html_st11 = PSTR("<div><i class=\"fas as fa-arrow-up\"></i><input placeholder=\"Topic out\" id=to name=\"mto\" value=\"");

    PGM_P html_st12 = PSTR("<div><i class=\"fas as fa-arrow-down\"></i><input type=checkbox placeholder=\"Domoticz\" id=cd name=\"mcd\" ");
    PGM_P html_st13 = PSTR("><label for=cd>Domoticz format</label>");
    PGM_P html_st14 = PSTR("<div><i class=\"fas as fa-arrow-down\"></i><input type=checkbox placeholder=\"Home asisstant\" id=cha name=\"mcha\" ");
    PGM_P html_st15 = PSTR("><label for=cd>Home assistant format</label>");
    PGM_P html_st16 = PSTR("</div>");
    PGM_P html_st17 = PSTR("<div>");

    PGM_P html_st7 = PSTR("<div><button type='submit'>Save</button></div>");

    String txt = getHTMLHeader();

    txt += getHTMLCSS();
    //txt += getHTMLLogo();delay(01);
    txt += html_h1; delay(01);


    String mh = mqtt_host;
    if (server->hasArg(c_mh))
        mh = server->arg(c_mh);
    String mp = String(mqtt_port);
    if (server->hasArg(c_mp))
        mp = server->arg(c_mp);
    String mu = mqtt_usr;
    if (server->hasArg(c_mu))
        mu = server->arg(c_mu);
    String mpw = mqtt_pass;
    if (server->hasArg(c_mpw))
        mpw = server->arg(c_mpw);
    String mti = mqtt_ti;
    if (server->hasArg(c_mti))
        mti = server->arg(c_mti);
    String mto = mqtt_to;
    if (server->hasArg(c_mto))
        mto = server->arg(c_mto);
    bool mcd = mqtt_domoticz;
    if (server->hasArg(c_mcd))
        mcd = server->arg(c_mcd).equals(c_on);
    bool mcha = mqtt_ha;
    if (server->hasArg(c_mcha))
        mcha = server->arg(c_mcha).equals(c_on);

    Serial.printf_P(PSTR("[wsc][mqttcfg]mqtt host:%s, port:%s "), mh.c_str(), mp.c_str());
    Serial.printf_P(PSTR(" topic in:%s, topic out:%s "), mti.c_str(), mto.c_str());

    txt += html_st4;          delay(01);
    txt += mh;              delay(01);
    txt += html_st5;          delay(01);

    txt += html_st6;          delay(01);
    txt += mp;
    txt += html_st5;          delay(01);

    txt += html_st8;          delay(01);
    txt += mu;
    txt += html_st5;          delay(01);

    txt += html_st9;          delay(01);
    txt += mpw;
    txt += html_st5;          delay(01);

    txt += html_st10;          delay(01);
    txt += mti;
    txt += html_st5;          delay(01);

    txt += html_st11;         delay(01);
    txt += mto;
    txt += html_st5;          delay(01);

    txt += html_st12;         delay(01);
    if (mcd) txt += c_checked;
    txt += html_st13;
    txt += html_st16;         delay(01);

    txt += html_st14;         delay(01);
    if (mcha) txt += c_checked;
    txt += html_st15;
    txt += html_st16;         delay(01);


    PGM_P html_st18 = PSTR("[%d]:<input type=edit id=alias%d name=ea%d value=\"%s\" placeholder=\"Index\" />");
    //txt += "<div></div>";
    txt += "<div><h4>Indexes</h4></div>";
    uint8_t objcnt = CProgObj::getProgObjListSize();
    for (uint8_t i = 0; i < objcnt; i++) {
        CProgObj* obj = CProgObj::getProgObjByIndex(i);
        if (obj->canJSON()) {

            String sI = String(i);
            uint16_t alias = findAliasByIdx(i);

            String sA;
            if (alias == 0xFFFF) sA = ""; else sA = String(alias);

            txt += html_st17;
            txt += obj->getClassName();
            char temp[500];
            sprintf_P(temp, html_st18, i, i, i, sA.c_str());
            //txt += "[" + sI + "]:<input type=edit id=alias" + sI + " name=ea" + sI + " value=" + sA + " placeholder=\"Index\" />"; 
            txt += String(temp);
            txt += html_st16;
        }

    }


    txt += html_st7;          delay(01);

    txt += "</form>";         delay(01);
    txt += "</body></html>";  delay(01);

    server->send(200, c_text_html, txt);
    //Serial.println( txt );
    delay(100);

}

void WiFiStationCtrl::handleSaveMQTTToFlash() {


    //for(int i = 0; i < server->args(); i++)
    //    Serial.printf_P(PSTR("h_arg %d %s = %s\r\n"), i, server->argName(i), server->arg(i));

    mqtt_host = server->arg("mh");
    String mp = server->arg("mp");
    mqtt_usr = server->arg("mu");
    mqtt_pass = server->arg("mpw");
    mqtt_port = mp.toInt();
    mqtt_ti = server->arg("mti");
    mqtt_to = server->arg("mto");
    mqtt_domoticz = false; if (server->hasArg("mcd")) mqtt_domoticz = server->arg("mcd").equals("on");
    mqtt_ha = false; if (server->hasArg("mcha")) mqtt_ha = server->arg("mcha").equals("on");

    Serial.printf_P(PSTR("[wsc][mqttcfg]mqtt host:%s, port:%s "), mqtt_host.c_str(), mp.c_str());
    Serial.printf_P(PSTR(" topic in:%s, topic out:%s "), mqtt_ti.c_str(), mqtt_to.c_str());


    global_aliasList.clear();
    uint8_t objcnt = CProgObj::getProgObjListSize();
    for (uint8_t i = 0; i < objcnt; i++) {
        CProgObj* obj = CProgObj::getProgObjByIndex(i);
        String ea = "ea" + String(i);
        if (server->hasArg(ea)) {

            alias_t alias;
            alias.idx = i;
            alias.alias = server->arg(ea).toInt();
            if (((!(alias.alias == 0))) || ((!(alias.alias == 0xFFFF))))
                global_aliasList.push_back(alias);
        }
    }

    delay(100);

    ///save_connparams_toFlash(ssid, psk);
    write2EEPROM();

    char temp[500];
    String ip = WiFi.localIP().toString();
    sprintf_P(temp, c_reset, ip.c_str());

    server->send(200, c_text_html, temp);
    delay(100);

    ESP.reset();
}

void WiFiStationCtrl::handleWiFiCfg() {

    //PGM_P html_logo = PSTR( "<div><img src=\"/logo\"></div>" );
    PGM_P html_h1 = PSTR("<div style=\"margin-bottom:2em;\"><h1>Select the station:</h1></div><form method=\"post\" action=\"/save\">");

    //PGM_P html_st1  = PSTR( "<div><i class=\"fas fa-wifi\"><a href=\"/wificfg?ssid=" );
    //PGM_P html_st2  = PSTR( "\">" );
    //PGM_P html_st3  = PSTR( "</a></i></div>" );

    PGM_P html_st1 = PSTR("<div class=ap><i class=\"fas fa-wifi\"></i><label onclick=\"c(this)\">");
    PGM_P html_st2 = PSTR("</label></div>");

    PGM_P html_st4 = PSTR("<div><i class=\"fas fa-user-secret\"></i><input placeholder=\"SSID\" id=s name=\"ssid\" value=\"");
    PGM_P html_st5 = PSTR("\"></div>");
    PGM_P html_st6 = PSTR("<div><i class=\"fas as fa-key\"></i><input placeholder=\"Password\" id=p name=\"psk\" value=\"");

    PGM_P html_st7 = PSTR("<div><button type='submit'>Save</button></div>");

    String txt = getHTMLHeader();

    txt += getHTMLCSS();

    //txt += getHTMLLogo();delay(01);
    txt += html_h1; delay(01);

    int n = WiFi.scanNetworks();
    if (n > 0) {

        for (int i = 0; i < n; ++i) {

            txt += html_st1;      delay(10);
            txt += WiFi.SSID(i);  delay(10);
            txt += html_st2;      delay(10);

        }
    }


    String ssid = String("");
    //if ( server->argName(0).equalsIgnoreCase( "ssid" ) ) 
    ssid = server->arg("ssid");

    Serial.printf_P(PSTR("[wsc][wificfg]ssid:%s\r\n"), ssid.c_str());

    txt += html_st4;          delay(01);
    txt += ssid;              delay(01);
    txt += html_st5;          delay(01);

    txt += html_st6;          delay(01);
    txt += html_st5;          delay(01);

    txt += html_st7;          delay(01);

    txt += "</form>";         delay(01);
    txt += "</body></html>";  delay(01);

    server->send(200, c_text_html, txt);
    //Serial.println( txt );
    delay(100);

}

void WiFiStationCtrl::handleNotFound() {

    PGM_P html_txt = PSTR("<div><h1>Not found</div></h1>");

    String txt = getHTMLHeader();

    txt += getHTMLCSS();
    //txt += getHTMLLogo();delay(01);
    txt += html_txt; delay(01);
    server->sendHeader("Location", "/", true);
    server->send(302, c_text_html, txt);

}

void WiFiStationCtrl::handleLogo() {

    /*static const uint8_t gif[] PROGMEM = {
      0x47, 0x49, 0x46, 0x38, 0x37, 0x61, 0x10, 0x00, 0x10, 0x00, 0x80, 0x01,
      0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0x2c, 0x00, 0x00, 0x00, 0x00,
      0x10, 0x00, 0x10, 0x00, 0x00, 0x02, 0x19, 0x8c, 0x8f, 0xa9, 0xcb, 0x9d,
      0x00, 0x5f, 0x74, 0xb4, 0x56, 0xb0, 0xb0, 0xd2, 0xf2, 0x35, 0x1e, 0x4c,
      0x0c, 0x24, 0x5a, 0xe6, 0x89, 0xa6, 0x4d, 0x01, 0x00, 0x3b
    };

    char gif_colored[sizeof(gif)];
    memcpy_P(gif_colored, gif, sizeof(gif));
    // Set the background to a random set of colors
    gif_colored[16] = millis() % 256;
    gif_colored[17] = millis() % 256;
    gif_colored[18] = millis() % 256;
    server->send(200, "image/gif", gif_colored, sizeof(gif_colored));*/

    /*PGM_P c_logo_png = PSTR("< img src = \"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr"
        "0AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAOxAAADsQBlSsOGwAABV9JREFUWIXtl1lsm1UWx3/n22zHW2zHcbPUA1VInC6E"
        "JUBRwjJFggJVBWIaaTodKRpBK1RgGECqEEgIIVrEAyoP0JQHeIBKwUWtQBWLkKhKgdGIMgKpmpayaOqkMzRO0uyL7e+7PMROb"
        "cdtoxbxAn/pPvice+/53XP0nXsNv+u3Lln0zGeU1tSWulwzjNET6xsGf1WAlmRqlaD2iMgqAJTaf4z4hiYfhjnZ94JSap2CD789E3"
        "+MLZL9RQESyZOPCrJDE9xOkV0p9TjIn0Vo1wAHUMie4xuWbvplAJJKT5A6ICJrBRABRy1cLIDKjzkwXj7eFX90sQDauRzNklq3LG"
        "CuPXD7Et5eE+MPPgND5hYYAg+2Brg6YoGcDc4c6N9bkqmnLxlAg+qmgMmygMnKsEVnzI0IWLqwuzPKwyuC9HRGaQ2a6GV51ITn"
        "Esm+zZcEIEo5U7nSnLs04Y2bo9wYcwPgMzV23xSlwWssrKU4r7Yk+/500QC2yFjx75BL581ba2mLuJjJOfT+MMHxkQxhl87rN0W"
        "p9eglmwmiizh7mvf233ZRAMWazDmsi1fRUm0xnnW4/3Ca5I8T/O3TNF8PzVLvNXitM0rA0tCKUiGIpSl7f8vek+0XDdA3kePYmSy"
        "NXoPhWZvuQwNUu3R618R4vj3M1s8HOTGa4Yqgya6OGixNSsohIn6B9xP7+psXD7BbmZqSjaenbU5N5WiPuvhpKsemgwM0B0xeXh3B"
        "0oU/1nt44spqNh8epH8yR1vExc7VEQyt9PsWJIptf3xFMtVwQYDGZMqTCPW9s9Snd714fZjVtW7+O55l48EBOmJutl8XRi/K872X"
        "eelu9nP/p2mGZ21urvOwvT2MJgsg4rrQe0EAn6jn2iLW+t41MRLVFsdGMvzl4AD3Xe7lqatDiCzsXd3Nfm5v9PDA4TRjGYe74162tVWXzBEApTrYfcQ8LwCKlZsTAcIuna8GZ+k+NMDW5QG2Lg8umFqsf6wM0lpt8dAXg2RsxaYmP1taAwign21W+9jSXnJX"
        "LABwhNdf+OYMO4+O8OBnaZ66KsTGJv95gwOICM9eGyJoaTz+ryFspXhkRZA7Gj3YCpTiyBDj3QvWVdqsdW/fnUo5Pduvi8Tvvcxb4huYtnkvNYlbE6IenasiFjGPMe/P2IrNn6Vp8Bo8e02IJ78c5kBqMpdjtv77rub0ogAAEntT7/Z01Ky/pc5zNjtKcddHP5"
        "EImvhNjfSMzdEzGXZ11LAq7JqfN5516D40wHejWXJzpx853hUPVYpjVDICoJSTLbv+JrKK09M5PlxbN2/r/WGCN06M89LqswB+U6O9xsWxkZJy6xRu7SKdqxFpOE5Fn1aWtKhbZ7rszijImJ+qBPABVh7kwgDKsRfVps+nQrtQds4GYkAYqALmP8VKJRDAxLb1Cr6Lk+PYQC"
        "3gyQcfZq4cdiUADXApJ3dJAMXPt2y6/9+AK2+eyUPMFIKVSwDDnhzr++fpGWxVWt9K1a5kG8s4KMCeHPvu5PN/3QlMAOPAFJArLKuUAQXk/t+745W3vDtWvP3jxA0RtyF1VTo+QyPjKHb9Z5SgpdHoMzgxmiHjKByl0Ira9KytmJ3NDP2vZ9s2O91/ChjLB5/JD6dw2nJp+RR5AItly93hznuWV"
        "MWW1evBmiXi8vgNfyiOaUY1w6xDNyOi62FdN5cELF0afTpVusaX6anp4U+SD53u2fY5MJLPQAawKXrHnqsRaflRePhqzH0+UrSmYDMAjYYmo+bWrlp"
        "raUu9GQhFJ7744OjQgV2ngOn8yFD6gOZ8AOUqDlxuK/cV4MmftjAqNovF/zVbvMr3rNyl8voZW4bN5ehjGIQAAAAASUVORK5CYII=\" >");*/
    server->send(200, "image/text", "", 0);
}

void WiFiStationCtrl::handleSaveToFlash() {

    String ssid = server->arg("ssid");
    String psk = server->arg("psk");

    Serial.printf_P(PSTR("[wsc]save to flash ssid:%s, psk:"), ssid.c_str());
    Serial.println(psk);
    delay(100);

    save_connparams_toFlash(ssid, psk);

    char temp[500];
    String ip = WiFi.localIP().toString();
    sprintf_P(temp, c_reset, ip.c_str());

    server->send(200, c_text_html, temp);
    delay(100);

    ESP.reset();
}

String WiFiStationCtrl::getHTMLHeader() {

    PGM_P html_header1 = PSTR(

        "<!DOCTYPE html>\n<html>\n\
    <meta charset='UTF-8'>\n\
    <meta name='format-detection' content='telephone=no'>\n\
    <meta name=\"viewport\" content=\"width=device-width, height=device-height, initial-scale=1, user-scalable=0, minimum-scale=1.0, maximum-scale=5.0\" />\n\
    <link rel=\"stylesheet\" \n\
      href=\"https://use.fontawesome.com/releases/v5.7.2/css/all.css\" \n\
      integrity=\"sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr\" \n\
      crossorigin=\"anonymous\" />\n\
  </head>\n\
  <body style=\"max-width: 561px;\">\n\
  <h4><div class=logo style=\"margin-bottom:1em;margin-left:-9em;display:inline-block;\">\n\
<img style=\"margin-bottom:-8px;\" src = \"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr\n\
0AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAOxAAADsQBlSsOGwAABV9JREFUWIXtl1lsm1UWx3/n22zHW2zHcbPUA1VInC6E\n\
JUBRwjJFggJVBWIaaTodKRpBK1RgGECqEEgIIVrEAyoP0JQHeIBKwUWtQBWLkKhKgdGIMgKpmpayaOqkMzRO0uyL7e+7PMROb\n\
cdtoxbxAn/pPvice+/53XP0nXsNv+u3Lln0zGeU1tSWulwzjNET6xsGf1WAlmRqlaD2iMgqAJTaf4z4hiYfhjnZ94JSap2CD789E3\n\
+MLZL9RQESyZOPCrJDE9xOkV0p9TjIn0Vo1wAHUMie4xuWbvplAJJKT5A6ICJrBRABRy1cLIDKjzkwXj7eFX90sQDauRzNklq3LG\n\
CuPXD7Et5eE+MPPgND5hYYAg+2Brg6YoGcDc4c6N9bkqmnLxlAg+qmgMmygMnKsEVnzI0IWLqwuzPKwyuC9HRGaQ2a6GV51ITn\n\
Esm+zZcEIEo5U7nSnLs04Y2bo9wYcwPgMzV23xSlwWssrKU4r7Yk+/500QC2yFjx75BL581ba2mLuJjJOfT+MMHxkQxhl87rN0W\n\
p9eglmwmiizh7mvf233ZRAMWazDmsi1fRUm0xnnW4/3Ca5I8T/O3TNF8PzVLvNXitM0rA0tCKUiGIpSl7f8vek+0XDdA3kePYmSy\n\
NXoPhWZvuQwNUu3R618R4vj3M1s8HOTGa4Yqgya6OGixNSsohIn6B9xP7+psXD7BbmZqSjaenbU5N5WiPuvhpKsemgwM0B0xeXh3B\n\
0oU/1nt44spqNh8epH8yR1vExc7VEQyt9PsWJIptf3xFMtVwQYDGZMqTCPW9s9Snd714fZjVtW7+O55l48EBOmJutl8XRi/K872X\n\
eelu9nP/p2mGZ21urvOwvT2MJgsg4rrQe0EAn6jn2iLW+t41MRLVFsdGMvzl4AD3Xe7lqatDiCzsXd3Nfm5v9PDA4TRjGYe74162t\n\
VWXzBEApTrYfcQ8LwCKlZsTAcIuna8GZ+k+NMDW5QG2Lg8umFqsf6wM0lpt8dAXg2RsxaYmP1taAwign21W+9jSXnJX\n\
LABwhNdf+OYMO4+O8OBnaZ66KsTGJv95gwOICM9eGyJoaTz+ryFspXhkRZA7Gj3YCpTiyBDj3QvWVdqsdW/fnUo5Pduvi8Tvvcxb4h\n\
uYtnkvNYlbE6IenasiFjGPMe/P2IrNn6Vp8Bo8e02IJ78c5kBqMpdjtv77rub0ogAAEntT7/Z01Ky/pc5zNjtKcddHP5\n\
EImvhNjfSMzdEzGXZ11LAq7JqfN5516D40wHejWXJzpx853hUPVYpjVDICoJSTLbv+JrKK09M5PlxbN2/r/WGCN06M89LqswB+U6O9xs\n\
WxkZJy6xRu7SKdqxFpOE5Fn1aWtKhbZ7rszijImJ+qBPABVh7kwgDKsRfVps+nQrtQds4GYkAYqALmP8VKJRDAxLb1Cr6Lk+PYQC\n\
3gyQcfZq4cdiUADXApJ3dJAMXPt2y6/9+AK2+eyUPMFIKVSwDDnhzr++fpGWxVWt9K1a5kG8s4KMCeHPvu5PN/3QlMAOPAFJArLKuUA\n\
QXk/t+745W3vDtWvP3jxA0RtyF1VTo+QyPjKHb9Z5SgpdHoMzgxmiHjKByl0Ira9KytmJ3NDP2vZ9s2O91/ChjLB5/JD6dw2nJp+RR5AItly93hznuWV\n\
MWW1evBmiXi8vgNfyiOaUY1w6xDNyOi62FdN5cELF0afTpVusaX6anp4U+SD53u2fY5MJLPQAawKXrHnqsRaflRePhqzH0+UrSmYDMAjYYmo+bWrlp\n\
raUu9GQhFJ7744OjQgV2ngOn8yFD6gOZ8AOUqDlxuK/cV4MmftjAqNovF/zVbvMr3rNyl8voZW4bN5ehjGIQAAAAASUVORK5CYII=\">\n\
"
);
    //PGM_P html_header2 = PSTR("");
    PGM_P html_header3 = PSTR("<head><title>http://");
    PGM_P html_header4 = PSTR(".local/</title>");

    String head = html_header3;
    head += String(hostName);
    head += html_header4;
    head += String(html_header1) + String(hostName);

    PGM_P html_bv2 = PSTR(".local</div><div><label style='font-size:xx-small;font-style:italic;'>ver. ");
    PGM_P html_bv3 = PSTR("</label></div></h4>");

    head += html_bv2;
    head += getBuildVersion();
    head += html_bv3;

    //head += String(html_header2);

    return head;

}

String WiFiStationCtrl::getHTMLCSS() {

    PGM_P html_css_style1 = PSTR(

        "  <style>\n\
  html {\n\
   font-family: Arial;\n\
   display: inline-block;\n\
   margin: 0px auto;\n\
   text-align: center;\n\
  }\n\
  h2 { font-size: 3.0rem; }\n\
  p { font-size: 3.0rem; }\n\
  .units { font-size: 1.2rem; }\n\
  .dht-labels{\n\
    font-size: 1.5rem;\n\
    vertical-align:middle;\n\
    padding-bottom: 15px;\n\
  }\n\
  .ap {margin-left:1.5em;display: flex;flex-direction:row;}\n\
  edit {margin-top:2em;}\n\
  button {background-color:#333344;color:white;padding:2.8rem 8.8rem;\n\
   text-decoration:none;margin:0.4em;cursor:pointer;text-size:xx-large;border-radius:2em;\n\
   margin-top:2em;}\n\
  i {color:#059e8a;}\n\
  .cpr{font-size:xx-small;}\n\
  a {padding-top:1.2rem;}\n\
  .lab {padding-top:1.2rem;}\n\
  .switch {position: relative;display: inline-block;width: 120px;height: 68px;}\n\
.switch input {display:none;}\n\
.slider {position: absolute;cursor: pointer;top: 0;left: 0;right: 0;bottom: 0;background-color: #ccc;\n\
-webkit-transition: .8s;transition: .8s;}\n\
.slider:before {position: absolute;content: \"\";height: 52px;width:52px;left:8px;bottom: 8px;background-color: white;\n\
  -webkit-transition: .8s;transition: .8s;}\n\
input:checked + .slider {background-color: #2196F3;}\n\
input:focus + .slider {box-shadow: 0 0 2px #2196F3;}\n\
input:checked + .slider:before{\n\
  -webkit-transform: translateX(52px);\n\
  -ms-transform: translateX(52px);\n\
transform: translateX(52px);}\n\
.slider.round {  border-radius: 68px;}\n\
.slider.round:before {border-radius: 50%;}\n\
.dev {margin-bottom: 1em;}\n\
</style>\n\
  <script>function c(l){\n\
  document.getElementById('s').value=l.innerText;\n\
  p=l.nextElementSibling.classList.contains('l');\n\
  document.getElementById('p').disabled = !p;\n\
  if(p)document.getElementById('p').focus();}\n\
function ag(u, cb) {var xp = new XMLHttpRequest();\n\
xp.onreadystatechange = function() {if (xp.readyState == 4 && xp.status == 200) {\n\
try {var dt = JSON.parse(xp.responseText);}catch (err) {return;}cb(dt);}};\n\
xp.open('GET', '/' + u, true);\n\
"

);

    PGM_P html_css_style2 = PSTR(" xp.send();} </script>");

    return String(html_css_style1) + String(html_css_style2);
}