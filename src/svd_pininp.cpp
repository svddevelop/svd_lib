/*
 esp8266 SVD-Framework. The class for simply input from
 any pin of SoC, like button, switch or one pins sensor.
 It could be use the interrupt or loop-process.

 Copyright (c) 2020 Sergej Pfaffenrot. All rights reserved.

 Based on ESP8266 EEPROM library, part of standard
 esp8266 core for Arduino environment by Ivan Grokhotkov.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "svd_pininp.h"
#include "svd_prjobj_cfgstruct.h"
//#include <common.h>
#include <Arduino.h>
#include <FunctionalInterrupt.h>
#include <ArduinoJson.h>


const char c_json[]    PROGMEM = "{\"name\":\"%s\",\"object\":\"pininp\",\"index\":%d,\"state\":%d,\"bmode\":%d,\"imode\":%d,\"pin\":%d}";
const char c_bmode[]   PROGMEM = "bmode";
const char c_imode[]   PROGMEM = "imode";

void CPinInp::setState(uint8_t a_state) {

    state_before = state;
    state = a_state;
}

String    CPinInp:: getJSON(){
  //If pin does not assigned then do not get something
  if ( pin == SVD_PO_NOTDEFINED )
      return "";
  
  char temp[ 500 ];
  //temp[0] = 0;
  //String s_bp = getBatteryPowerS();
 
  snprintf_P ( temp, sizeof(temp), c_json, getChipName().c_str(), index, state, button_mode, int_mode, pin );
  return String(temp);
}

bool   CPinInp::putJSON( String a_json){
  
   bool ret = false;
   StaticJsonDocument<550> doc;
   deserializeJson(doc, a_json.c_str(), a_json.length() );

  //const char* name = doc["name"].as<char*>();
  //String docName = String( doc["name"].as<char*>() );

  String key = String(c_index); 
  if ( doc.containsKey( key ) ){
     int idx = doc[ key ].as<int>();
     ret = (idx == getIndex() );
   }
   if ( ! ret ) return ret;
    
   key = String(c_pin);
   if (doc.containsKey(key)) {
       uint8_t j_pin = doc[key].as<int>();
       setPin(j_pin);
   }

   //uint8_t val = getJsonDocValue( &doc, c_state, getState() );
   key = String(c_state);
   if ( doc.containsKey( key ) ){
     uint8_t j_state = doc[ key ].as<int>(); 
     setState( j_state );
   }

   key = String(c_bmode);
   if ( doc.containsKey( key ) ){
     uint8_t j_bm = doc[ key ].as<int>(); 
     setButtonMode( j_bm );
   }

   key = String(c_imode); 
   if ( doc.containsKey( key ) ){
     uint8_t j_im = doc[ key ].as<int>(); 
     setInterruptMode( j_im );
   }
   
   return ret;
}

void CPinInp::setPin( int8_t a_pin){

    pin = a_pin;
    if ( pin > SVD_PO_NOTDEFINED ){

        pinMode( pin, INPUT_PULLUP );
        int8_t im = getInterruptMode();
        if (im > 0) {

            Serial.printf_P(PSTR("%d[CPinInp] set Interrupt%d(pin:%d)\r\n"), index, int_mode, pin);
            attachInterrupt(digitalPinToInterrupt(pin)
                , std::bind(&CPinInp::internal_handle, this)
                , im
            );
        }

        setState( digitalRead(pin) );
        Serial.printf_P( PSTR("%d[CPinInp]Set reaction Pin:%d, Mode:%d State:%d\r\n"), index, pin, im, getState() );
    }
}

void  CPinInp::pininp_loop(){

    PGM_P c_sermsg = PSTR("%d[CPinInp{%1}]Reaction CPinInp Pin:%d, state=%d, state_before=%d\r\n");

  if (( flip )&(int_mode > 0)) {
    
      flip = false;
  
      //setState(digitalRead(pin));
      Serial.printf_P(c_sermsg, index, 1, pin, state, state_before);

      callBind( SVD_CONTEXT_BUTTONSTATE );

      CALL_EVENT_PIN_( pin );
  }

  if (getInterruptMode() < 0) {

      uint8_t dr = digitalRead(pin);

      if (getState() != dr) {

          setState(dr);
          Serial.printf_P(c_sermsg, index, 2, pin, state, state_before);

          callBind(SVD_CONTEXT_BUTTONSTATE);

          CALL_EVENT_PIN_(pin);
      }
  }
  
}

ICACHE_RAM_ATTR void CPinInp::internal_handle() {

    if (int_mode > 0) {

        uint8_t digRead = digitalRead(pin);
        Serial.printf_P(PSTR("%d[CPinInp]Interrupt(IntMode:%d) Pin:%d, digRead=%d\r\n"), index, int_mode, pin, digRead);
        setState(digRead);

        flip = true;
    }
  
}

void CPinInp::setButtonMode( uint8_t aBtnMode ){

    button_mode = aBtnMode;
    setPin(pin);
}

void CPinInp::setInterruptMode( int8_t aIntMode ){

    int_mode = aIntMode;
}

uint8_t    CPinInp::getState( ){

  //if ( pin > SVD_PO_NOTDEFINED ){
  //    Serial.printf_P( PSTR("%d[CPinInp]getState(p=%d)=%d\r\n"), index, pin, state );
  //}
  return state;
}

obj_options_t    CPinInp::getObjOptions(){

  obj_options_t ret;
  
  ret.deviceKind                = SVD_CPROGOBJKIND_CPININP;
  ret.obj.cbutton.InterruptMode = getInterruptMode();
  ret.obj.cbutton.ButtonMode    = getButtonMode();
  ret.obj.cbutton.Pin           = pin;
  ret.obj.haveHTML              = canShowHtml();
   
  return ret;
}

int32               CPinInp::getStateInt(uint8_t a_idx) {

    if (a_idx == 0) return getState();

    return SVD_PO_NOTDEFINED;
}

float               CPinInp::getStateFloat(uint8_t a_idx) {

    if (a_idx == 0) return getState();
    return SVD_PO_NOTDEFINED;
}

int32               CPinInp::getStateBeforeInt(uint8_t a_idx) {

    if (a_idx == 0) return state_before;
    return SVD_PO_NOTDEFINED;
}

float               CPinInp::getStateBeforeFloat(uint8_t a_idx) {

    if (a_idx == 0) return state_before;
    return SVD_PO_NOTDEFINED;

}

