/*
 esp8266 SVD-Framework. The class for simply output over
 any pin of SoC, like relay, summer or LED.

 Copyright (c) 2020 Sergej Pfaffenrot. All rights reserved.

 Based on ESP8266 EEPROM library, part of standard
 esp8266 core for Arduino environment by Ivan Grokhotkov.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef SVD_PINOUT_H_
#define SVD_PINOUT_H_

#include "svd_prjobj.h"

#define   SVD_REL_STATE_OFF     0
#define   SVD_REL_STATE_ON      1
#define   SVD_REL_STATE_SWITCH  2
#define   SVD_REL_DEF_STATE     SVD_REL_STATE_OFF

class CPinOut : public CProgObj {
  protected:
     uint8_t            state = SVD_REL_DEF_STATE;
     uint8_t            state_before = SVD_REL_DEF_STATE;
     bool               inverted_signal = true;
     int8_t             pin = SVD_PO_NOTDEFINED;
     bool               doByDefaultOn = false;
protected:
    void                objectBindCall(uint8_t a_sender, bind_options_t bind_options);
public:

    bool                is_dev_out() { return true; };
    void                setState(uint8_t a_state);
    uint8_t             getState() { return state; };
    uint8_t             switch_state();
    String              getJSON();
    bool                putJSON(String a_json);
    uint8_t             getValueOn();
    uint8_t             getValueOff();
    void                setInvertedSignal(bool a_ps);
    void                setPin(int8_t a_pin);

    void                setRelayOn();
    void                setRelayOff();
    bool                isRelayOn();
    bool                isRelayOff();
    obj_options_t       getObjOptions();

    int32               getStateInt        (uint8_t a_idx);
    float               getStateFloat      (uint8_t a_idx);
    int32               getStateBeforeInt  (uint8_t a_idx);
    float               getStateBeforeFloat(uint8_t a_idx);

    String              getHTML();
    bool                canJSON() { return true; };
    String              getClassName() { PGM_P name = PSTR("PinOut"); return String(name); };


     CPinOut( bool a_canShowHTML = false, int8_t a_pin = SVD_PO_NOTDEFINED)
        { 
          CProgObj::newProgObj(); 
          html_report = a_canShowHTML;
          setPin( a_pin ); 
        } 
     CPinOut(bool a_canShowHTML, int8_t a_pin, bool aInvertedSignal)
        { 
          CProgObj::newProgObj(); 
          html_report = a_canShowHTML;
          setPin( a_pin );
          setInvertedSignal( aInvertedSignal ); 
        }
     CPinOut(bool a_canShowHTML, int8_t a_pin, bool aInvertedSignal, bool setAsDefaultOn)
        { 
          CProgObj::newProgObj();
          html_report = a_canShowHTML;
          setPin( a_pin );
          setInvertedSignal( aInvertedSignal ); 
          if ( setAsDefaultOn ){ 

              Serial.printf_P ( PSTR("%d[R] setAsDefaultOn"), index);
              doByDefaultOn = true;
              setRelayOn(); 
          }
          else  
              setRelayOff(); 
        } 
};

#endif
