/*
 esp8266WiFi SVD-Framework. CBME280 class.

 Copyright (c) 2020 Sergej Pfaffenrot. All rights reserved.

 Based on ESP8266 EEPROM library, part of standard
 esp8266 core for Arduino environment by Ivan Grokhotkov.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "svd_bme280.h"
#include "svd_prjobj_cfgstruct.h"

const char c_json[]  PROGMEM = "{\"name\":\"%s\",\"object\":\"BME280\",\"index\":\"%d\""
                               ",\"temp\":[%s]"
                               ",\pin\":%d%s}";
//const char c_stype[] PROGMEM = "stype";



//         https://diyprojects.io/getting-started-i2c-bus-arduino-esp8266-esp32-wire-library/#.YZpvbaLMKUk

void CBME280::init(){

    bme.begin();
 //   count = ds.getDeviceCount();
 //   temp = new float[count];
 //   for (uint8_t i = 0; i < count; i++) {
 //       temp[i] = 0;
 //   }

}

String    CBME280:: getJSON(){
  //If pin does not assigned then do not get something
  if ( pin == SVD_PO_NOTDEFINED )
      return "";
  //*/

//  getStateFloat( SVD_CBME280_IDX_TEMP );
  
  char ttemp[ 500 ];
  ttemp[0] = 0;
  String s_bp = getBatteryPowerS();

  getData();
  String s_tmp = "";
 // for (uint8_t i = 0; i < count; i++) {
 //     s_tmp += temp[i];
 //     s_tmp += ",";
 // }
  s_tmp += "0";
   
  snprintf_P ( ttemp, sizeof(ttemp), c_json
                  , getChipName().c_str()
                  , index, s_tmp.c_str() 
                  , pin
                  , s_bp.c_str()
              );
  return String(ttemp);
}

/*bool   CBME280::putJSON( String a_json){
  
   bool ret = false;
   StaticJsonDocument<550> doc;
   deserializeJson(doc, a_json.c_str(), a_json.length() );

  //const char* name = doc["name"].as<char*>();
  //String docName = String( doc["name"].as<char*>() );

  if ( a_json.indexOf( "\"" + String( c_index) + "\"") > 0){
     int idx = doc[ String(c_index) ].as<int>();
     ret = (idx == getIndex() );
   }
   if ( ! ret ) return ret;
    

   //Serial.printf( c_show_name, idx );
   if ( a_json.indexOf( "\"" + String( c_state) + "\"") > 0){
     uint8_t j_state = doc[ String(c_state) ].as<int>(); 
     setState( j_state );
   }
   
   if ( a_json.indexOf( "\"" + String( c_bmode) + "\"") > 0){
     uint8_t j_bm = doc[ String(c_bmode) ].as<int>(); 
     setButtonMode( j_bm );
   }
    
   if ( a_json.indexOf( "\"" + String( c_imode) + "\"") > 0){
     uint8_t j_im = doc[ String(c_imode) ].as<int>(); 
     setInterruptMode( j_im );
   }
   
   return ret;
}*/

void CBME280::getData(){
  
        bme.begin();

        float f = 0;// bme.getTempCByIndex(i);
            if (isnan(f)) f = 0;
            else
                temp = f;

            f = 0;// bme.getTempCByIndex(i);
            if (isnan(f)) f = 0;
            else
                hum = f;
            
            f = 0;// bme.getTempCByIndex(i);
            if (isnan(f)) f = 0;
            else
                press = f;
            
            f = 0;// bme.getTempCByIndex(i);
            if (isnan(f)) f = 0;
            else
                alti = f; 
            
            Serial.printf_P(PSTR("[%d]CBME280::getData() T:%2.1f H:%2.1f P:%3.0f A:%3.1f\r\n"), index, temp, hum, press, alti);
        
  
}

float      CBME280::getStateFloat(uint8_t a_index){

    float ret = 0;
    uint32_t rest = millis() - latest_millis;

    if (     (!( pin == SVD_PO_NOTDEFINED ))
           &&(( rest > SVD_BME280_MESSTRESHOLD )||(latest_millis == 0))
        ){

          getData();

          latest_millis = millis();
    }
    if ( a_index == SVD_BME280_IDX_TEMP) 
        ret = temp;
    if (a_index == SVD_BME280_IDX_HUM) 
       ret = hum;
    if (a_index == SVD_BME280_IDX_PRESS) 
       ret = press;
    if (a_index == SVD_BME280_IDX_ALTIT) 
       ret = alti;

    return ret; 
}

float      CBME280::getTemperature(){
  
   return getStateFloat(SVD_BME280_IDX_TEMP);
}
     
float      CBME280::getHumidity() {

    return getStateFloat(SVD_BME280_IDX_HUM);
}

float      CBME280::getPressure() {

    return getStateFloat(SVD_BME280_IDX_PRESS);
}

float      CBME280::getAltitude() {

    return getStateFloat(SVD_BME280_IDX_ALTIT);
}


obj_options_t    CBME280::getObjOptions(){

    obj_options_t       ret;
    //ret.deviceKind      = SVD_CPROGOBJKIND_CBME280;
    ret.obj.haveHTML    = canShowHtml();
    return              ret;
  
}




String  CBME280::getHTML() {

    PGM_P c_html = PSTR(

        "<div><i class=\"fas fa-thermometer-half\">Temperature</i>:<span id='t%d[%d]'> %3.1f </span></div>\r\n"

        );

    PGM_P c_script  = PSTR(

 
        "<script>"
        "setTimeout(function(){\r\n"       
        "ag('json?obj=%d&sid=%d', function(d){\r\n"       
        "document.getElementById('ds%d[%d]').innerHTML=d['temp'] + ' °C';\r\n"
        "}, 5*60000);\r\n"
        "});</script>\r\n" 

    );

    String ret = "";
    if (extHandlerHTML != NULL)
        extHandlerHTML(&ret);
    else {

        getData();

        //uint8_t idx = getIndex();

        char ttemp[500];
 //       for (uint8_t i; i < count; i++) {
//
//            snprintf_P(ttemp, sizeof(ttemp), c_html, index, i, this->temp, index);
//            ret += ttemp;
//        }
        //Здесь не все так гладко с индексами. нужно менять js
        snprintf_P(ttemp, sizeof(ttemp), c_script, index,  index);

    }
    return ret;
}