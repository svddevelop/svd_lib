/*
 esp8266WiFiStationControl.h - esp8266 do a manage of your 
 ssid/password, automatically connection to external access point
 or start the internal acces point with conficuration web-server. 
 
 Copyright (c) 2020 Sergej Pfaffenrot. All rights reserved.
 
 Based on ESP8266 EEPROM library, part of standard
 esp8266 core for Arduino environment by Ivan Grokhotkov.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#ifndef ESP8266WIFISTATIONCTRL_H_
#define ESP8266WIFISTATIONCTRL_H_

#define WFSC_DNS_PORT_          53
#define WFSC_UDCOUNT_           16

#include <user_interface.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <vector>
#include <ESP8266HTTPUpdateServer.h>



class WiFiStationCtrl;

typedef struct {const char * addr; void* handlerProc; } html_handler_t;
typedef void(*wificonnect_t)(WiFiStationCtrl* wifisc);

typedef void (*eeprom_rw_func)(void* ptr, uint8_t ptrsize);

typedef struct __attribute__((packed)) { uint8_t idx ; uint16_t alias; } alias_t;

using WebServer = ESP8266WebServer;


  class WiFiStationCtrl {

    public:

      WiFiStationCtrl();
              
      String getHostName();
      
      const char* getFlashSSID();
      
      const char* getFlashPsk();

      bool compare_ssid( const char* a_ssid);

      bool get_connparams_fromFlash();

      bool is_have_ssid();

      bool make_connection_fromFlash();

      void save_connparams_toFlash(String ssid, String passwd);

      void start_accesspoint_with_confSrv(bool withCaptive);

      void wifi_connect(  const bool a_PrintDot );

      void handleWiFiCfg();

      bool is_ssid_active();
      bool set_ssid_active();

      void setup(const bool a_PrintDot, bool a_withCaptive);
      void setup_direct(const char* a_ssid, const char* a_pwd, const bool a_PrintDot);

      String getHTMLHeader();

      String getHTMLCSS();

      String getBuildVersion();

      bool   setWiFiSSID( const char* a_ssid);
      bool   setWiFiPWD( const char* a_pwd);

      //ESP8266WebServer* startWebServer( int port );

      //ESP8266WebServer* getWebServer();	

      void webServerSetup( WebServer*  );
      WebServer* getWebServer();

      void setTimeout(uint a_timeout);	

      uint8_t addHandler( html_handler_t a_handler);

      void setOnWiFiConnect(wificonnect_t a_handel_proc) {
          local_onwificonnect = a_handel_proc
              ;
      };

      String   getMQTTHost();
      void     setMQTTHost(String a_host);
      uint16_t getMQTTPort();
      void     setMQTTPort(uint16_t a_port);
      String   getMQTTUsr();
      void     setMQTTUsr(String a_usr);
      String   getMQTTPw();
      void     setMQTTPw(String a_pass);
      String   getMQTTTopicIn();
      void     setMQTTTopicIn(String a_str);
      String   getMQTTTopicOut();
      void     setMQTTTopicOut(String a_str);

      uint64_t getEEPROMUserData(uint8_t a_idx);
      void     setEEPROMUserData(uint8_t a_idx, uint64_t a_data);
      void     write2EEPROM();

      bool     isPoorChip();

      WiFiClient wifiClient;

      static  std::vector<alias_t> global_aliasList;
      static  uint16_t findAliasByIdx(uint8_t aIdx);
      static  uint8_t findIdxByAlias(uint16_t aAlias);

    protected:
        void handleMqtt();

        void handleSaveMQTTToFlash();

        void handleNotFound();

        void handleLogo();

        void handleSaveToFlash();

        void handleJSON();

        void handleObjects();

        void handleUpdate();

        String getHTMLLogo();

    private:

      String   mqtt_host;
      uint16_t mqtt_port = 1883;

      uint8_t eeprom_usr_data[WFSC_UDCOUNT_];

      String   mqtt_usr;
      String   mqtt_pass;
      String   mqtt_ti ;
      String   mqtt_to ;
      bool     mqtt_domoticz;
      bool     mqtt_ha;

      String hostName;
      
      station_config wifi_config;

      bool have_ssid;

      WebServer* server;

      uint timeout;

      std::vector<html_handler_t> ext_handlers;

      wificonnect_t local_onwificonnect = NULL;

      ESP8266HTTPUpdateServer httpUpdater;


      bool  read2EEPROM();
      void WriteToUserData(uint8_t a_ud[]);
      void ReadFromUserData(uint8_t a_ud[]);

      void readEEPROMaliasList();
      void writeEEPROMaliasList();

  };

  static WiFiStationCtrl* WIFICTRL;

#endif


 
