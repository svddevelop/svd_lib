/*
 esp8266 SVD-Framework. Software Timer.

 Copyright (c) 2020 Sergej Pfaffenrot. All rights reserved.

 Based on ESP8266 EEPROM library, part of standard
 esp8266 core for Arduino environment by Ivan Grokhotkov.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "svd_timer.h"
#include "svd_prjobj_cfgstruct.h"
#include "svd_pinout.h"
#include "svd_pininp.h"

#define TIME_PERIOD_MUL         (100)

obj_options_t    CTimer::getObjOptions(){

   obj_options_t ret;
   ret.deviceKind              = SVD_CPROGOBJKIND_CTIMER ;
   ret.obj.ctimer.timePeriod   =  getTimePeriod() ;   // 
   ret.obj.ctimer.SetDefaultOn =  asDefaultOn;
   ret.obj.haveHTML            = canShowHtml();
   return ret;
}

void       CTimer::setState( uint8_t a_state){

    if (( a_state == SVD_CTIMERSTATE_ENABLE)&& ( state != a_state ))
      startTime = millis();
      
    state = a_state;
    callBind( SVD_CONTEXT_NOTSET );
}

uint8_t    CTimer::switch_state(){

  Serial.printf_P( PSTR("%dCTimer:switch_state()"), index);
  if ( isTimerOn() )
    setTimerOff();
  else
    setTimerOn();

  return state;
  
}

String     CTimer::getJSON(){

  return    "{\"type\":\"CTimer\",\"index\":" + String( getIndex() ) 
          + ",\"period\":" + String( getTimePeriod() )
          + ",\"state\":" + String( state )
          + "}";
}

bool       CTimer::putJSON(String a_json) {

    bool ret = false;

    return ret;
}

/* The period must been in 100mS */
void       CTimer::setTimePeriod( uint16 a_timePeriod ){ 
  
  time_period = a_timePeriod * TIME_PERIOD_MUL;
  Serial.printf_P(PSTR("[%d]CTimer:TimePeriod %d mS"), index, time_period);
  
}


uint16_t   CTimer::getTimePeriod(){

    return time_period / TIME_PERIOD_MUL;
}

     
void       CTimer::setTimerOn(){

    if ( isTimerOn() ) return;
    
    setState( SVD_CTIMERSTATE_ENABLE );
    Serial.printf_P( PSTR("[%d]CTIMER setTimerOn(%d mS)\r\n"), getIndex(), time_period);
}

void       CTimer::setTimerOff(){

    if ( isTimerOff() ) return;
    
    setState( SVD_CTIMERSTATE_DISABLE );
    Serial.printf_P( PSTR("[%d]CTIMER setTimerOff(%d mS)\r\n"), getIndex(), time_period );
}

bool       CTimer::isTimerOn(){

   return state == SVD_CTIMERSTATE_ENABLE;
}

bool       CTimer::isTimerOff(){

   return state == SVD_CTIMERSTATE_DISABLE;
}

void       CTimer:: objectBindCall( uint8_t a_sender, bind_options_t bind_options){

   Serial.printf_P( PSTR("[%d]TIMER objectBindCall(%d)\r\n"), index, index);
   debugBindOptions( bind_options );
   CProgObj* sender = getProgObjByIndex(a_sender);
   obj_options_t sender_options = sender->getObjOptions();
   
   /*if ( sender_options.deviceKind == SVD_CPROGOBJKIND_CPININP){
    
     CPinInp* btn_ = (CPinInp*)sender;
     if ( btn_->getButtonMode() == SVD_BUTTONMODE_SWITCH ) {}
   }

   if ( sender_options.deviceKind == SVD_CPROGOBJKIND_CPINOUT){
    
     CPinOut* rly_ = (CPinOut*)sender;
     if ( rly_->getState() == SVD_BUTTONMODE_SWITCH ) {}
   }*/

     switch(bind_options.cmd){
          case SVD_CTIMERSTATE_ENABLE:
                          setTimerOn();
                          break;
                          
          case SVD_CTIMERSTATE_DISABLE:
                          setTimerOff();
                          break;        
     }
   
   
}

void       CTimer:: timer_loop(){

    if ( isTimerOn() ){

       uint32_t check = millis() - startTime;
       if ( check > time_period){

           Serial.printf_P( PSTR("[%d]CTimer:timer_loop()\r\n"), index);

           setTimerOff();
           
           callBind( SVD_CONTEXT_TIMER );

           CALL_EVENT_PIN_( SVD_PO_NOTDEFINED );
       }
         
    }
}
