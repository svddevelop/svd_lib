/*
 esp8266WiFi SVD-Framework. DS1820 class.

 Copyright (c) 2020 Sergej Pfaffenrot. All rights reserved.

 Based on ESP8266 EEPROM library, part of standard
 esp8266 core for Arduino environment by Ivan Grokhotkov.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef SVD_DS1820_H_
#define SVD_D1820_H_


#include "svd_prjobj.h"
#include "svd_prjobj_cfgstruct.h"
#include <OneWire.h>
#include <DallasTemperature.h>

#define SVD_CDS1820_MESSTRESHOLD   1000

class CDS1820 : public CProgObj {
  protected:
     int8_t             pin = SVD_PO_NOTDEFINED;
     OneWire            oneWire;
     DallasTemperature  ds;
     float*             temp;
     uint8_t            count;
     uint32_t           latest_millis = 0;
   public:

     bool               is_dev_out(){ return false;};
     bool               is_dev_in(){ return true;};
     String             getJSON();
     obj_options_t      getObjOptions();
     void               setPin( int8_t a_pin){ pin = a_pin;  };
     DallasTemperature* getDS(){ return &ds; }

     float              getStateFloat(uint8_t a_index);
     float              getTemperature(uint8_t a_index);
     void               getData();
     void               init();

     uint8_t            getCount() { return count; };

     String             getHTML();
     bool               canJSON() { return true; };


     CDS1820( int8_t a_pin)
        : pin(a_pin), oneWire(a_pin), ds(&oneWire)
        { 
          CProgObj::newProgObj(); 
          html_report = true;
          init();
        } 

};
#endif
