/*
 esp8266 SVD-Framework. Software Timer.

 Copyright (c) 2020 Sergej Pfaffenrot. All rights reserved.

 Based on ESP8266 EEPROM library, part of standard
 esp8266 core for Arduino environment by Ivan Grokhotkov.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef SVD_TIMER_H
#define SVDF_TIMER_H

#include "svd_pininp.h"
#include "svd_prjobj_cfgstruct.h"

#define SVD_CTIMERSTATE_DISABLE   0
#define SVD_CTIMERSTATE_ENABLE    1
#define SVD_CTIMERSTATE_EVENT     2

class CTimer : public CProgObj {
  protected:
     uint8_t    state = 0;
     uint32_t   time_period = 0;
     bool       asDefaultOn = false;
     uint32_t   startTime;
     bool       can_json = false;
   protected:
     void objectBindCall( uint8_t a_sender, bind_options_t bind_options);
   public:
     void       timer_loop();
   public:

     bool       is_dev_out(){ return true;};
     bool       is_dev_in() { return true;};
     void       setState( uint8_t a_state);
     uint8_t    switch_state();
     String     getJSON();
     bool       putJSON(String a_json);
     void       setTimePeriod( uint16 a_timePeriod ); // The period must been in 100mS 

     uint16_t   getTimePeriod(); // The period must been in 100mS 
     
     void       setTimerOn();
     void       setTimerOff();
     bool       isTimerOn();
     bool       isTimerOff();
     obj_options_t    getObjOptions();
     bool       canJSON() { return can_json; };
     void       setCanJSON(bool aVal) { can_json = aVal; };
     String     getClassName() { PGM_P name = PSTR("Timer"); return String(name); };

     CTimer(uint16_t a_timePeriod = 0)
        { 
          CProgObj::newProgObj(); 
          setTimePeriod( a_timePeriod );
        } 
     CTimer( uint16_t a_timePeriod, bool a_setTimerOn )
        { 
          CProgObj::newProgObj(); 
          setTimePeriod( a_timePeriod );
          asDefaultOn = a_setTimerOn;
          if ( a_setTimerOn )
            setTimerOn(); 
        } 
};



#endif
