/*
 esp8266WiFi SVD-Framework. DS1820 class.

 Copyright (c) 2020 Sergej Pfaffenrot. All rights reserved.

 Based on ESP8266 EEPROM library, part of standard
 esp8266 core for Arduino environment by Ivan Grokhotkov.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef SVD_BME280_H_
#define SVD_BME280_H_


#include "svd_prjobj.h"
#include "svd_prjobj_cfgstruct.h"
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

#define SVD_BME280_MESSTRESHOLD   1000
#define SVD_BME280_IDX_TEMP             0
#define SVD_BME280_IDX_HUM              1
#define SVD_BME280_IDX_PRESS            2
#define SVD_BME280_IDX_ALTIT            3

class CBME280 : public CProgObj {
  protected:
     int8_t             pin = SVD_PO_NOTDEFINED, pin_sck = SVD_PO_NOTDEFINED;
     Adafruit_BME280    bme;
     float              temp, hum, press, alti;
     uint32_t           latest_millis = 0;
   public:

     bool               is_dev_out(){ return false;};
     bool               is_dev_in(){ return true;};
     String             getJSON();
     obj_options_t      getObjOptions();
     void               setPin( int8_t a_pin){ pin = a_pin;  };
     Adafruit_BME280*   getBME(){ return &bme; }

     float              getStateFloat(uint8_t a_index);
     float              getTemperature();
     float              getHumidity();
     float              getPressure();
     float              getAltitude();
     void               getData();
     void               init();


     String             getHTML();

     CBME280( int8_t a_pin_data, int8_t a_pin_sck)
        : pin(a_pin_data), pin_sck(a_pin_sck)
        { 
          CProgObj::newProgObj(); 
          html_report = true;
          init();
        } 

};
#endif
