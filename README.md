
The framework must  simplify the issues of the IOT-Devices based on ESP8266 Module. It has the following features:

+ WiFi Control. Each IOT-device must be connected to the network WIFI-router. If the router is unreachable, the device will  automatically reconnect with them, but any other features, such as reaction on the sensors, and buttons stay independent (asynchronous mode).

+ WiFI Control uses the last connect parameters (SSID/PSK) for restore connection and do not rewrite each time these information on the flash.

+ WiFi Control check on the start a list of the reachable SSID's, when latest connected SSID is not reachable, start the Access Point for settings of the parameters (user must be connected to the Access Point, open the browser on a URL 'http://192.168.4.1/wificfg', and choose the SSID).

## HTTP Access to device
 
+ Is it possible to change the SSID configuration: http://<devicename_or_IP>/wificfg

+ Is it possible to read full configuration of IOT-device with URL 'http://<devicename_or_IP>/json?obj=-1'. So you get the JSON like this:
```json
{
   "name":"IOT10241268",
   "sensors":[
      {"name":"IOT10241268","object":"pinout","index":0,"state":1,"inverted":1,"pin":2,"Battery":63.00},
      {"name":"IOT10241268","object":"pininp","index":1,"state":1,"bmode":0,"imode":-1,"pin":0},
      {"type":"CTimer","index":2,"period":60000,"state":0},
      {"name":"IOT10241268","object":"dht","index":"3","temp":"21.0","humidity":"73.0","stype":"11","Battery":63.00},
      {"name":"IOT10241268","state":1}
   ]
}
```
or you can get settings for each device standalone (with URL '.../json?obj=<index>').

+ you can change some properties with URL like this: '.../obj?obj=1&state=0'.

+ Update Firmware can you do it at the http://<devicename_or_IP>/upd. You need to choice a local *.bin file and press button "Update Firmware". Warning! You might update the firmware with other function and can not to reset it back. In this situation need to flashe the firmware over serial interface. 

## MQTT Access to device

+ you can get full configuration over MQTT. You need to send follow message:
```json
{"name":"IOT10241268","sensors":"*"}
```

or a broadcast for all IOT-devices in your network, subscribed on the topic:
        
        
```json
{"name":"*","sensors":"*"}
```

____

## Classes functions and events

### WiFiStationCtrl

``String getHostName()`` - retrive the name of the SoC. Thi is combination of text 'IOT' and Chip ID and used as DNS-name with suffix '.local';
      
``const char* getFlashSSID()`` -read and get the last selected SSID from the FLASH-Memory;
      
``const char* getFlashPsk()`` - read anf get the PSK-key of the last selected SSID from then FLASH-memory;

``bool compare_ssid( const char* a_ssid)`` - this make the comparasion current selected SSID with any other (a_ssid). true - are idetical, false - aren't identical;

``bool get_connparams_fromFlash()`` - this function read the connection parameters (SSID and PSK) from the FLASH-memory into internal variables. All data can be read with ``getFlashSSID()``and ``getFlashPsk()``; 

``bool is_have_ssid()`` - true - if the SSID presetted in the FLASH-memory and active ;

``bool make_connection_fromFlash()`` - read the SSID and PSK from the FLASH and try to connect with the SSID;

``void save_connparams_toFlash(String ssid, String passwd)`` - saved presetted ssid and psk as the connect parameter;

``void start_accesspoint_with_confSrv( bool withCaptive )`` - started access point. The SSID of AP used as getHostName(). withCaptive==true : The AP started with Captive Portal functionality (any http request will retrieve wifi settings page only); 

``void wifi_connect( const bool a_PrintDot )`` - try to connect to presetted SSID. a_PrintDot==true : do print '.' in the Serial interface; 

``void setup(const bool a_PrintDot, bool a_withCaptive)`` - extracted wifi_connect(). a_withCaptive==true: start with Captive portal;

``String getHTMLHeader()`` - get HTML-Document header;

``String getHTMLCSS()`` - get HTML-CSS-section;	

``void webServerSetup( WebServer*  )`` - define user created WebServer into WiFiStationCtrl. If not defined, will be created automatically;

``WebServer* getWebServer()`` - get current WebServer

``void setTimeout(uint a_timeout)`` - set timeout to show Access Point. It works in situation with connection to the pressetted SSID and AP;	

``uint8_t addHandler( onst char * addr, void* handlerProc)`` - add new own user-defined http handler;

``void setOnWiFiConnect(wificonnect_t a_handel_proc)`` - do a callback, when WiFi connection successed;

``WiFiClient wifiClient`` - inplemented WiFiClient;


#### part to support of CMQTT.

``String   getMQTTHost()`` - 

``void     setMQTTHost(String a_host)``

``uint16_t getMQTTPort()``

``void     setMQTTPort(uint16_t a_port)``

``String   getMQTTUsr()``

``void     setMQTTUsr(String a_usr)``

``String   getMQTTPw()``

``void     setMQTTPw(String a_pass)``

``String   getMQTTTopicIn()``

``void     setMQTTTopicIn(String a_str)``

``String   getMQTTTopicOut()``

``void     setMQTTTopicOut(String a_str)``

``uint64_t getEEPROMUserData(uint8_t a_idx)`` - get user-defined data (2 x Int64);

``void     setEEPROMUserData(uint8_t a_idx, uint64_t a_data)`` - get user-defined data (2 x Int64);

``void     write2EEPROM()`` - it could be done for SoC with FLASH-memory > 512KB;


### CProgObj - the base class for all extends.

#### statics methods

``static CProgObj* getProgObjByIndex(uint16_t a_idx)`` - find registered object by index;

``static String getChipName()`` - get chip name ('IOT' + chip id);

``static uint8_t getCMQTT_index()`` - find index of object of CMQTT;

``static String getJSONAll()`` - get the JSON of all objects;

``static void global_set_pin(CProgObj* a_obj, uint8_t a_pin, uint8_t a_val)``

``static  void global_put_JSON( String a_json)`` - process the JSON to change some features of objects;

``static String getHTMLAll()`` - get a HTML's of all objects;

#### virtuals

`` virtual String getHTML()`` - get the HTML-Text of current object;

``virtual int32 getStateInt( uint8_t a_idx)`` 

``virtual float getStateFloat( uint8_t a_idx)``

``virtual int32 getStateBeforeInt(uint8_t a_idx)``

``virtual float getStateBeforeFloat(uint8_t a_idx)``

``virtual uint8_t switch_state()`` - the function changing the current state to next one (if state exists)
 
``virtual bool putJSON( String a_json)`` - process the JSON to change some property of object;

``virtual String getJSON()`` - get the JSON-text of current object;

#### usual

``void setOnEventPin( event_pin_t aEventProc)`` - a user-defined handler, when state changed then call this;

``bool canShowHtml()`` - if true, this object can be show with ``getHTMLAll()``;


``uint16_t getIndex()`` - get a index of the current object;

``void setIndex( uint16_t a_idx)`` - set a index of the current object;

``void setExternalHTMLhandler(event_handle_t a_handler)`` - set the user defined handler to do the HTML-text of object; 


### CPinInp

#### costructors

``CPinInp( int8_t a_pin = SVD_PO_NOTDEFINED )``

``CPinInp( int8_t a_pin, int8_t a_intMode)``

#### usual

``void setPin(int8_t a_pin)``

``void setButtonMode(uint8_t aBtnMode)``

``uint8_t getButtonMode()``

``void setInterruptMode( int8_t aIntMode)`` - could be call with aIntMode as SVD_NOINTERRUPT, SVD_BUTTON_DOWN,SVD_BUTTON_UP, SVD_BUTTON_ANY;

``int8_t getInterruptMode()``

``bool isButtonOn()``

``bool isButtonOff()``




The class 

### CPinOut

### CTimer

### CMQTT

### CDHT





