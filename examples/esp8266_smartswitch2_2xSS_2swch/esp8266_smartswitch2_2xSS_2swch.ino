/*
 * 
 * The device has follows sensors in configuration:
 * 
 * relay0               0     - relay1
 * relay2               2     - relay2
 * btn4                 4     - switch1 fro relay1
 * btn5                 5     - switch for relay2
 * LED14                14    - LED WiFiError      
 * DHT13                13    - DHT
 * timer_dht                  - each 2 minutes send json-report over MQTT
 * MQTT               
 */

  #define PIN_RELAY_0_       0
  #define PIN_RELAY_2_       2
  #define PIN_BUTTON_4_      4
  #define PIN_BUTTON_5_      5
  #define PIN_SETUP_BUTTON_  PIN_BUTTON_2_
  #define PIN_BUZZER_        4
  #define PIN_LED_CTRL_      14
  #define PIN_DS1820_        5
  #define LED_CE_ACTIVE      0
  #define LED_CE_INACTIVE    1
  #define PIN_DHT_13_        13
  #define BTN_IS_SWITCH_     1

//#define NOREL0_
//#define NOREL2_
//#define NODHT13_
//#define NOSWITCH4_
//#define NOSWITCH5_
//#define NOLEDCONERR_



#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ESP8266SSDP.h>
#include <Esp.h>
#include <pgmspace.h>

#include "svd_pinout.h"
//#include "svd_pininp.h"
#include "svd_timer.h"
#include "svd_mqtt.h"
#include "svd_dht.h"


#include "esp8266WiFiStationCtrl.h"

typedef struct{
  uint8_t check1: 1;
  uint8_t check2: 1;
  uint8_t check3: 1;
  uint8_t check4: 1;
  uint8_t rest:   4;
} bits8_t;

typedef union{
  bits8_t bits_;
  uint8_t int_;
} userdata_t;

//==========================================================================

const char c_mqtt_host[]   PROGMEM  = "192.168.0.2";
const char c_mqtt_topin[]  PROGMEM  = "iot/in";
const char c_mqtt_topout[] PROGMEM  = "iot/out";
#define c_mqtt_port   1883
//==========================================================================

#ifndef NOREL0_
CPinOut relay0(true, PIN_RELAY_0_, true, false);
#endif
#ifndef NOREL2_
CPinOut relay2(true, PIN_RELAY_2_, true, false);
#endif

//==========================================================================

#ifndef NODHT13_
CDHT    dht13(PIN_DHT_13_, 11);
#endif 


#ifndef NODHT13_
  #define TIMER_DTH_MQTTREPORT_ (2*60* 10)
CTimer  timer_dht(TIMER_DTH_MQTTREPORT_, true);
#endif

//==========================================================================


WebServer* server;
WiFiStationCtrl wifictrl =  WiFiStationCtrl(); 
//==========================================================================

CMQTT   mqtt( &wifictrl.wifiClient
              , 550
              //, wifictrl.getMQTTHost()
              //, wifictrl.getMQTTPort()
              //, wifictrl.getMQTTTopicOut()
              //, wifictrl.getMQTTTopicIn()
              );

//==========================================================================


#ifndef NOSWITCH4_
CPinInp btn4(PIN_BUTTON_4_, SVD_NOINTERRUPT);
#endif
#ifndef NOSWITCH5_
CPinInp btn5(PIN_BUTTON_5_, SVD_NOINTERRUPT);
#endif

//==========================================================================

bool isStartOn[2] = {false, false};
bool isSwitch[2] = {false, false};


//////////////////////////////////////////////////////////////////////////////////////////////////////  

const char* c_html_footer PROGMEM = "</body></html>";
const char* c_html_hrefs PROGMEM =  "<div style=\"border-top:1px solid #9EC1D4;padding-top:1em;\">"
                                    "<span style=\"float:left;padding-left:10em;\"><a href=\"/setup\"><i class=\"fas fa-cogs\"></i></a></span>"
                                    "<span><a href=\"/wificfg\"><i class=\"fas fa-wifi\"></i></a></span>"
                                    "<span style=\"float:right;padding-right:10em;\"><a href=\"/mqttcfg\"><i class=\"fas fa-broadcast-tower\"></i></span></a></div>";
const char* c_html_timeoutredirect PROGMEM = "<script>document.ready(window.setTimeout(location.href = \"http://%s/?\",5000));</script>";

void httpHandleRoot(){

  PGM_P header_txt = PSTR("text/html");

  String html = wifictrl.getHTMLHeader();  
       
  html += wifictrl.getHTMLCSS(); 

  html += CPinOut::getHTMLtext();

  html += c_html_hrefs;
  
  html += CPinOut::getHTMLfooter();
       
  Serial.print( F("\r\nhandleRoot:") );
       
  server->send(200, header_txt, html);
}

userdata_t hh_check_arguments(  ){

  PGM_P txt_on = PSTR("on");
  PGM_P txt_chk1 = PSTR("chk1");
  PGM_P txt_chk2 = PSTR("chk2");
  PGM_P txt_chk3 = PSTR("chk3");
  PGM_P txt_chk4 = PSTR("chk4");

  userdata_t ud;
  ud.int_ = wifictrl.getEEPROMUserData(0);
    
  ud.bits_.check1 = 0;if (server->hasArg("chk1") ) ud.bits_.check1 = server->arg("chk1").equals("on");
  ud.bits_.check2 = 0;if (server->hasArg("chk2") ) ud.bits_.check2 = server->arg("chk2").equals("on");
  ud.bits_.check3 = 0;if (server->hasArg("chk3") ) ud.bits_.check3 = server->arg("chk3").equals("on");
  ud.bits_.check4 = 0;if (server->hasArg("chk4") ) ud.bits_.check4 = server->arg("chk4").equals("on");
  Serial.printf_P( PSTR(":3:ud.int_=%2.2x"), ud.int_);
  
  wifictrl.setEEPROMUserData(0, ud.int_);
  return ud;
}

void httpHandlePinCfg(){

  userdata_t ud;

  //for(int i=0; i<server->args(); i++)
  //  Serial.printf_P( PSTR("h_arg %d %s = %s\r\n"), i, server->argName(i), server->arg(i));

  ud.int_ = wifictrl.getEEPROMUserData(0);
  
  PGM_P html1 = PSTR(
  "<form method=\"post\" action='/setupsave' style='text-align:left;'>\n\
  <div><input type=checkbox id=1 name=\"chk1\" \n\
"
  );
  
  PGM_P html2 = PSTR(
  "><label for=1>On start first button must be 'on'</label></div>\n\
  <div><input type=checkbox id=2 name=\"chk2\" \n\
"
  );
  
  PGM_P html3 = PSTR(
  "><label for=2>On start second button must be 'on'</label></div>\n\
  <div><input type=checkbox id=3 name=\"chk3\" \n\
"
  );
  
  PGM_P html4 = PSTR(
  "><label for=3>The first button is the switch</label></div>\n\
  <div><input type=checkbox id=4 name=\"chk4\" \n\
"
  );
  
  PGM_P html5 = PSTR(
  "><label for=4>The second buttonis the switch</label></div>\n\
  <div><input type=\"submit\" value=\"Save\"></div></form>\n\
"
  );
  

  PGM_P header_txt = PSTR("text/html");
  PGM_P html_checked = PSTR("checked");
  
  String html = wifictrl.getHTMLHeader();
  html += wifictrl.getHTMLCSS();
  html += html1;
  if ( ud.bits_.check1 > 0 )  
    html += html_checked;
  html += html2;
  if ( ud.bits_.check2 > 0 )  
    html += html_checked;
  html += html3;
  if ( ud.bits_.check3 > 0 )  
    html += html_checked;
  html += html4;
  if ( ud.bits_.check4 > 0 )  
    html += html_checked;
  html += html5;

  html += c_html_hrefs;
  
  html += c_html_footer;
  
  server->send(200, header_txt, html);  
}

void httpHandlePinCfgSave(){
  
  PGM_P header_txt = PSTR("text/html");
  PGM_P html = PSTR("<body>SAVED and reboot");
  PGM_P html_end = PSTR("</body>");
  //PGM_P html_loc = PSTR(".local");

  userdata_t ud;
  ud = hh_check_arguments(  );
  
  wifictrl.write2EEPROM();

  String txt = html;
  char temp[500];
  String ip = WiFi.localIP().toString();
  //ip += html_loc;
  sprintf(temp, c_html_timeoutredirect, ip.c_str() );
  txt += String(temp);
  txt += html_end;

  server->send(200, header_txt, txt);  

  delay(1000);
  ESP.reset();
}

void httpHandleUpdNew(){

  PGM_P header_txt = PSTR("text/html");

  String html = wifictrl.getHTMLHeader();  
       
  html += wifictrl.getHTMLCSS(); 
  
  PGM_P html1 = PSTR(  
"<form method='POST' action='/upd' enctype='multipart/form-data'>\n\
Firmware:<br>\n\
<input type='file' accept='.bin,.bin.gz' name='firmware'>\n\
<input type='submit' value='Update Firmware'>\n\
</form></body></html>\n\
"
);
  PGM_P html2 = PSTR("<label>ver. ");
  PGM_P html3 = PSTR("</label><br>");

  html += html2;
  String nr = String(__DATE__);
  nr.remove(6, 1);
  nr.remove(1, 3);
  
  html += nr;
  nr = String(__TIME__);
  nr.remove(2, 1);
  nr.remove(5, 1);
  
  html += nr;

  html += html3;
  html += html1;
  server->send(200, header_txt, html);

}

//////////////////////////////////////////////////////////////////////////////////////////////////////  

void btn_click( uint8_t index, uint8_t pin){

   Serial.printf_P( PSTR("button_click(%d)\r\n"), index );

   //const char* m1 = "b4 s=1";
   //const char*  m2 = "b4 s=0";

  //сделать для свитча 
  #ifndef NOREL0_
   if ( pin == PIN_BUTTON_4_ ){

     #ifdef BTN_IS_SWITCH_
     
           if ( btn4.getState() == 1 ){
              relay0.setRelayOff();
           }
           else{
              relay0.setRelayOn();
           }
     
     #else 
          relay0.switch_state();
     #endif
     mqtt.sendMQTT( &btn4 );   
   }
  #endif

  #ifndef NOREL2_ 
   if ( pin == 5 ){

//      #ifdef BTN_IS_SWITCH_

           if ( btn5.getState() == 1 ){
            
              relay2.setRelayOff();
              
           }
           else{
            
              relay2.setRelayOn(); 
           }
          
//      #else
      
//           relay2.switch_state();

//      #endif
      mqtt.sendMQTT( &btn5 );  
   } 
  #endif
}

void on_wificonnect(WiFiStationCtrl* wifisc){
  
  
}

void timer_click( uint8_t index, uint8_t pin){

  #ifdef SUMMERENABLE
  summ12.setRelayOn();
  #endif

  #ifndef NODHT13_
  if ( index == timer_dht.getIndex()){

    timer_dht.setTimerOff();
    timer_dht.setTimerOn();

    mqtt.sendMQTT(&dht13);

  }
  
  #endif
    
}

void relay_click( uint8_t index, uint8_t pin){

  #ifndef NOREL0_
  if ( index == relay0.getIndex()){
    
    mqtt.sendMQTT(&relay0);

  }
  #endif

  #ifndef NOREL2_
  if ( index == relay2.getIndex()){
    
    mqtt.sendMQTT(&relay2);

  }
  #endif

  #ifndef NODHT13_
    mqtt.sendMQTT(&dht13);
  #endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////  
  
void setup(){

  userdata_t ud;
  ud.int_ = wifictrl.getEEPROMUserData(0);
  isStartOn[0] =   ud.bits_.check1 > 0;
  isStartOn[1] =   ud.bits_.check2 > 0;
  isSwitch[0]  =   ud.bits_.check3 > 0;
  isSwitch[1]  =   ud.bits_.check4 > 0;


  Serial.printf_P( PSTR("isStartOn:%d %d; isSwitch: %d %d\r\n"), isStartOn[0], isStartOn[1], isSwitch[0], isSwitch[1]);

  #ifndef NODHT13_
  timer_dht.setOnEventPin( &timer_click );
  timer_dht.setTimerOn();

  #endif
  
  #ifndef NOREL0_
  pinMode(0, OUTPUT);
  relay0.setOnEventPin( &relay_click );
  if ( ! isStartOn[0] )
     relay0.setInvertedSignal(isStartOn[0]);
  if ( &isSwitch[0] ){
    relay0.setRelayOff();
  }
  else {
    relay0.setRelayOff();
  }
  #endif

  #ifndef NOREL2_
  relay2.setOnEventPin( &relay_click );
  if ( ! isStartOn[1] )
     relay2.setInvertedSignal(isStartOn[1]);
  if ( &isSwitch[1] ){
    relay2.setRelayOff();
  }
  else{
    relay2.setRelayOff();
  }
  #endif

  
  delay(1);
  Serial.begin(115200);

  //wifictrl.setMQTTTopicIn( "iot/in" );
  //wifictrl.setMQTTTopicOut( "iot/out" );
  wifictrl.setOnWiFiConnect( &on_wificonnect );
  
//  timer0.setOnEventPin( &timer_click );
//  timer1.setOnEventPin( &timer_click );
//  timer2.setOnEventPin( &timer_click );
//  timer2.setTimerOn();

  #ifndef NOSWITCH4_
  btn4.setOnEventPin( &btn_click );
  #endif
  #ifndef NOSWITCH4_
  btn5.setOnEventPin( &btn_click );
  #endif

  //
  //https://github.com/esp8266/Arduino/issues/5912
  //

  delay(10);

  //Serial.printf_P( PSTR("sDevName=%s timer1=%s\r\n"), CProjObj::getChipName().c_str(), timer1.getChipName().c_str() ); 
 
  server = new WebServer(80);
  server->begin( );
  server->on("/", httpHandleRoot);
  server->on("/setup", httpHandlePinCfg);
  server->on("/setupsave", httpHandlePinCfgSave);
  //server->on("/firmwareupdate",httpHandleUpdNew);
  
  wifictrl.webServerSetup( server );
  wifictrl.setTimeout( 180 ); // if SSID haven't active then start Soft access point on the 180 sec.
  wifictrl.setup( true, true ); 

  //##############################


  String s = wifictrl.getMQTTHost();
  if (s.length() > 0)
     mqtt.setHost( s );
     
  uint16_t port = wifictrl.getMQTTPort();
  if ((port > 0)&(port < 65500))
     mqtt.setPort( port );
  else{
    wifictrl.setMQTTPort(c_mqtt_port);
    mqtt.setPort( c_mqtt_port );
  }
     
  s = wifictrl.getMQTTHost();
  if (s.length() > 0)
     mqtt.setHost( s );
  else{
    wifictrl.setMQTTHost( String(c_mqtt_host) );
    mqtt.setHost( c_mqtt_host );
  }
     
  s = wifictrl.getMQTTTopicIn();
  if (s.length() > 0)
     mqtt.setSubTopic( s );
  else{
      mqtt.setSubTopic( String(c_mqtt_topin) );
      wifictrl.setMQTTTopicIn(c_mqtt_topin);
  }
     
  s = wifictrl.getMQTTTopicOut();
  if (s.length() > 0)
     mqtt.setPubTopic( s );
  else{
      mqtt.setPubTopic( String(c_mqtt_topout) );
      wifictrl.setMQTTTopicOut(c_mqtt_topout);
  }
     
  mqtt.mqtt_begin();
  mqtt.doMQTTSetting();

  //mqtt.sendMQTT(&relay2);
}


static bool temp_report_on_startup = false;

void loop() {
  
  CProgObj::global_loop();
  
  delay(1); 

  server->handleClient();
  delay(1); 

  //loop_check_wifi_for_led();

  if ( mqtt.getMQTTClient()->connected() ) {

    #ifndef NOLEDCONERR_
    digitalWrite( PIN_LED_CTRL_, LED_CE_INACTIVE);
    #endif
    
    if ( ! temp_report_on_startup ){

        temp_report_on_startup = true;

        Serial.println( F("temp_report_on_startup") );
        
        #ifndef NOREL0_
        mqtt.sendMQTT(&relay0);
        #endif
        
        #ifndef NOREL2_
        mqtt.sendMQTT(&relay2);
        #endif

        #ifndef NODHT13_
        mqtt.sendMQTT(&dht13);
        #endif
    }
  }else{

    #ifndef NOLEDCONERR_
    digitalWrite( PIN_LED_CTRL_, LED_CE_ACTIVE);
    #endif
    
  }
}
