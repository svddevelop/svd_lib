/*
 Example for ESP8266 SVD-Framework. 
 
 Copyright (c) 2020 Sergej Pfaffenrot. All rights reserved.
 
 Based on ESP8266 EEPROM library, part of standard
 esp8266 core for Arduino environment by Ivan Grokhotkov.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ESP8266SSDP.h>
#include <Esp.h>
#include <pgmspace.h>

#include "svd_pinout.h"
#include "svd_pininp.h"
#include "svd_timer.h"
#include "svd_mqtt.h"
#include "svd_dht.h"


#include "esp8266WiFiStationCtrl.h"

#define BATHROOMCFG_
#define DESKLAMPCFG_


#define TIMER1ENABLE
#define TIMER2ENABLE
#define SUMMERENABLE


#ifdef DESKLAMPCFG_
  #undef BEDROOMCFG_
  #undef TIMER1ENABLE
  #undef SUMMERENABLE
#endif

#define TIMER_TEST_15S          15000
#define TIMER_10min             (10 * 60 * 1000)
#ifdef DESKLAMPCFG_
  #undef TIMER_TEST_15S
  #undef TIMER_10min
  #define TIMER_10min           (1 * 60 * 1000)
#endif
#define TIMER_minus10sek        (TIMER_10min - 1*1000)

const char c_mqtt_host[] PROGMEM  = "192.168.1.2";
const char c_mqtt_topin[] PROGMEM = "iot/in";
const char c_mqtt_topout[] PROGMEM= "iot/out";
#define c_mqtt_port   1883

WebServer* server;
WiFiStationCtrl wifictrl =  WiFiStationCtrl(); 



CPinOut relay2(true, 2, true, true);


//CPinInp button0(0);
CPinInp button0(0, SVD_NOINTERRUPT);

#ifdef TIMER1ENABLE
CTimer  timer1(TIMER_minus10sek, true);
#endif
#ifdef TIMER2ENABLE
CTimer  timer2(TIMER_10min, true);
#endif
CDHT    dht13(13, 11);
#ifdef SUMMERENABLE
CPinOut summ12(false, 12, true, false);
#endif
CMQTT   mqtt( &wifictrl.wifiClient
              , 550
              //, wifictrl.getMQTTHost()
              //, wifictrl.getMQTTPort()
              //, wifictrl.getMQTTTopicOut()
              //, wifictrl.getMQTTTopicIn()
              );

//////////////////////////////////////////////////////////////////////////////////////////////////////  


void httpHandleRoot(){

  PGM_P header_txt = PSTR("text/html");

  String html = wifictrl.getHTMLHeader();  
       html += wifictrl.getHTMLCSS(); 
       //html += html_txt;
       html += CPinOut::getHTMLAll();
  Serial.print("\r\nhandleRoot:");
  //Serial.println(html);
       
  server->send(200, header_txt, html);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////  
#define BUTTON
void button_click( uint8_t index, uint8_t pin){

   #ifdef BUTTON
   if ( button0.getState() == 0 ){
    
     Serial.printf_P( PSTR("button_click(%d, %d)\r\n"), index, pin );
     #ifdef TIMER2ENABLE
     timer2.setTimerOn();
     #endif
     relay2.setRelayOn();
  
  
     mqtt.sendMQTT( &relay2 );
  
     mqtt.sendMQTT( &dht13 );

   }else{
     #ifdef TIMER2ENABLE
     timer2.setTimerOff();
     #endif
     relay2.setRelayOff();     
   }
   #else

   if ( button0.getStateInt(0) == HIGH ){

      if ( button0.getStateBeforeInt(0) == LOW ){
        
         Serial.println( F("EXIST") );
      }
   }else{
      if (  button0.getStateBeforeInt(0) == HIGH ){
        
         Serial.println( F("NOT EXIST") );
      }
   }
   #endif
}
void timer1_click( uint8_t index, uint8_t pin){

  #ifdef SUMMERENABLE
  summ12.setRelayOn();
  #endif
    
}

void timer2_click( uint8_t index, uint8_t pin){

   Serial.printf_P( PSTR("timer_click(%d, %d)\r\n"), index, pin );
   relay2.setRelayOff();

   #ifdef SUMMERENABLE
   //summ12.stopTon();
   summ12.setRelayOff();
   #endif
  
   mqtt.sendMQTT( &relay2 );

   mqtt.sendMQTT( &dht13 );   
}

void on_wificonnect(WiFiStationCtrl* wifisc){
  
  
}


///////////////////////////////////////////////////////////////////////////////////////////////////

void setup() {
  Serial.begin(115200);
  Serial.println( F("\r\n\r\nWelcome!\r\n") );

  Serial.printf( "h:%f\r\n", dht13.getStateFloat(1)  );
  Serial.printf( "t:%f\r\n",  dht13.getStateFloat(0)  );

  //wifictrl.setMQTTTopicIn( "iot/in" );
  //wifictrl.setMQTTTopicOut( "iot/out" );
  wifictrl.setOnWiFiConnect( &on_wificonnect );
  
  button0.setOnEventPin( &button_click );
  
  #ifdef TIMER1ENABLE
  timer1.setOnEventPin( &timer1_click );
  #endif
  #ifdef TIMER2ENABLE
  timer2.setOnEventPin( &timer2_click );
  timer2.setTimerOn();
  #endif

  
  //
  //https://github.com/esp8266/Arduino/issues/5912
  //

  delay(10);

  //Serial.printf_P( PSTR("sDevName=%s timer2=%s\r\n"), CProjObj::getChipName().c_str(), timer2.getChipName().c_str() ); 
 
  server = new WebServer(80);
  server->begin( );
  server->on("/", httpHandleRoot);
  
  wifictrl.webServerSetup( server );
  wifictrl.setTimeout( 180 ); // if SSID haven't active then start Soft access point on the 180 sec.
  wifictrl.setup( true, true ); 

  //##############################


  String s = wifictrl.getMQTTHost();
  if (s.length() > 0)
     mqtt.setHost( s );
     
  uint16_t port = wifictrl.getMQTTPort();
  if ((port > 0)&(port < 65500))
     mqtt.setPort( port );
  else{
    wifictrl.setMQTTPort(c_mqtt_port);
    mqtt.setPort( c_mqtt_port );
  }
     
  s = wifictrl.getMQTTHost();
  if (s.length() > 0)
     mqtt.setHost( s );
  else{
    wifictrl.setMQTTHost( String(c_mqtt_host) );
    mqtt.setHost( c_mqtt_host );
  }
     
  s = wifictrl.getMQTTTopicIn();
  if (s.length() > 0)
     mqtt.setSubTopic( s );
  else{
      mqtt.setSubTopic( String(c_mqtt_topin) );
      wifictrl.setMQTTTopicIn(c_mqtt_topin);
  }
     
  s = wifictrl.getMQTTTopicOut();
  if (s.length() > 0)
     mqtt.setPubTopic( s );
  else{
      mqtt.setPubTopic( String(c_mqtt_topout) );
      wifictrl.setMQTTTopicOut(c_mqtt_topout);
  }
     
  mqtt.mqtt_begin();
  mqtt.doMQTTSetting();

  mqtt.sendMQTT(&relay2);
}


#define _MQTT_UPDATE_PERIOD   (mqtt_update_interval * 60*1000)

uint32_t mqtt_update_interval_millis = 0;



static bool last_wifi_status = true;
void loop_check_wifi_for_led(){
/*
  if ( wifi_led_pin > _PIN_NOT_DEFINED ){

     pinMode( wifi_led_pin, OUTPUT );
     bool wifi_status = (( WiFi.status() == WL_CONNECTED )/*&( mqtt_client.connected() )*//* );
     digitalWrite(wifi_led_pin, ! wifi_status);

     if ( last_wifi_status != wifi_status){
      
        Serial.printf_P( PSTR("WiFiStatus:%d\r\n"), wifi_status); 
        last_wifi_status = wifi_status;
        
     }
  }*/
}

static bool temp_report_on_startup = false;


void loop() {

  CProgObj::global_loop();
  
  delay(1); 

  //---------------------------------------------------
    
  //loop_wifi_1();
  
//  loop_wifi_3();
  delay(1); 

  //---------------------------------------------------

//  if ( is_mDNS_allow )
//    MDNS.update();
  delay(1); 


  server->handleClient();
  delay(1); 

  //loop_check_wifi_for_led();

  if ( mqtt.getMQTTClient()->connected() )
    if ( ! temp_report_on_startup ){

        temp_report_on_startup = true;
        mqtt.sendMQTT(&dht13);
        mqtt.sendMQTT(&relay2);
    }


}

/*
 * 
 * 
 * нужно добавить:
 *    
 *    =  прикрутить PIR.
 *    =  в классах добавить сохранение предыдущего состояния объекта
 *       и возможность доступа из стравнения с предыдущим значением.
 *    =  отладить для установки у детей: без пищалки, с сенсорной кнопкой на 0 и PIR
 *    
 * 30.10.21 Создал страницу конфигурации mqtt, записи данных в eeprom и загрузки с параметрами.   
 *          для esp  с 512К параметры не сохраняются, но берет дефолтные.
 *    
 * 25.10.21 Подправлена работа с PinInp.
 *          Пробую разные конфигурации в ino с  изоляциец по дефайнам.
 * описал конфигурации
 *              PARTICLE1CFG    - частичная конфигурация для света без переделок,
 *                                при старте свет сразу горит
 *                                используется одно реле на пине2
 *    
 * 23.10.21 отладил полностью html+css+js - все работает, показывает и переключает как надо.
 *          
 *    
 * 22.10.21 создал главную страницу для smart_switch с красивой кнопкой и управление по js-ajax.
 *          создал хэндлер для возврата json по http-запросу.
 *          создал хэндлер для установки свойст объекта через http-запрос. 
 *    
 * 21.10.21 добавил в конфигурацию web server /upd
 *           putJSON нужно добавить пин для установки
 *    
 * 20.10.21 Рефакторинг. CRelay -> CPinOut; CButton -> CPinInp    
 * 
 *     
 * 20.10.21 Настроил для работы с WiFiStationControl. Там еще изменил
 *          DnsServer - при соединении сразу подключает /wificfg
 * 
 * = вероятно пищалку по таймеру - щелчек за минуту до выключения. 
 *          Сделал на основе реле.
 *          
 * 16.10.21 Добавил еа старт, по появлении соединения mqtt, отправку состояний
 *          реле и dht13.
 *          
 *          
 * 15.10.21 Отладка DHT. Посмтоянно крэшится при нажатии на кнопку.
 *          Опытным путем с анализом стэка и кода определил, что рушится на 
 *          каком нибудб delay() или yeld(). По запросу оказалось, что 
 *          expressif имеет проблемы с перечисленным в обработке прерываний пинов.
 *          Рекомендации - размещать функцию прерывния в памяти, а не во флэш.
 *          Рекомендация не помогла и сделал обработку через loop.  Сразу все заработало.
 *          
 *          по отправке mosquitto_pub -h 127.0.0.1 -t "domoticz/in" -m '{"name":"IOT10241294","sensors":"?"}'
 *          показывает конфигурацию всех устройств.
 *          
 * 
 * 
 * 
 * 14.10.21 Отладил CMQTT. Теперь исключение не выбрасывает. И соединяется точно.
 *          Теперь работатет еще и putJSON()
 *          Battery
 * 
 * 
 * 13.10.21 сделал промежуточную версию выключателя для служ. комнат
 *    = при включении автоматически включается
 *    = при включении запускается таймер на 10мин для выключения
 *    = при нажатии на кнопку происходит выключение
 *    = при смене статуса реле посылается mqtt
 *    
 */
